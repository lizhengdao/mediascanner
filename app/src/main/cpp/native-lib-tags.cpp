/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma ide diagnostic ignored "EmptyDeclOrStmt"

//#include <string>
#include <cinttypes>
#include <cstdlib>
#include <cerrno>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>

// Android
#include <android/log.h>

// advanced taglib interface
#include <mpeg/id3v2/id3v2tag.h>
#include <mpeg/id3v1/id3v1tag.h>
#include <mpeg/id3v2/frames/attachedpictureframe.h>
#include "mpegfile.h"
#include <mp4/mp4file.h>
#include <ogg/flac/oggflacfile.h>
#include <ogg/vorbis/vorbisfile.h>
#include <flac/flacfile.h>

#include "native-lib-utils.h"
#include "native-lib.h"
#include "native-lib-tags.h"


/************************************************************************************
 *
 * properties helper
 *
 ***********************************************************************************/
static void handleProps(audioFileInfo *info, TagLib::AudioProperties *props)
{
    if (props != nullptr)
    {
        info->duration = props->lengthInMilliseconds();
        LOGD("   %-40s :  %d ms\n", "length", info->duration);
        LOGD("   %-40s :  %d kb/s\n", "bit rate", props->bitrate());
        LOGD("   %-40s :  %d Hz\n", "sample rate", props->sampleRate());
        LOGD("   %-40s :  %d\n", "channels", props->channels());
    }
}


/************************************************************************************
 *
 * write cover picture
 *
 * pic_type is 1 (jpeg) or 2 (png)
 *
 * return: 0: OK, -1: cannot create, -2: cannot write
 *
 ***********************************************************************************/
static int writePicture(Context *c, TagLib::ByteVector *data, int pic_type, const char *destination_path)
{
    int rc;
    char buf[MAXPATH + 1];
    strcpy(buf, destination_path);
    strcat(buf, "/");
    strcat(buf, album_picture_base_fname);
    strcat(buf, (pic_type == 1) ? ".jpg" : ".png");
    LOGW("    creating picture file \"%s\"\n", buf);
    int fd = open(buf, O_CREAT + O_EXCL + O_WRONLY, 0666 /* rw-rw-rw- */);
    if (fd >= 0)
    {
        size_t size = data->size();
        ssize_t written = write(fd, data->data(), size);
        if (written < size)
        {
            LOGE("    file write failed on \"%s\" (errno %d)\n", buf, errno);
            rc = -2;
        }
        else
        {
            // call Java from C++
            jstring jpath = c->jenv->NewStringUTF(buf);
            // scale the picture, but ignore any problems here as we cannot do anything about it.
            (void) c->jenv->CallIntMethod(c->jobj, c->jmethodScalePicture, jpath, 0 /* 0: is created picture */);
            c->jenv->DeleteLocalRef(jpath);
            rc = 0;
        }
        close(fd);
    }
    else
    {
        LOGE("    file creation failed on \"%s\" (errno %d)\n", buf, errno);
        rc = -1;
    }

    return rc;
}


/************************************************************************************
 *
 * mp3 or flac:
 * Handle picture, if it is a cover picture and it is jpg or png.
 * Write it, if destination_path is not null.
 *
 * return: 0: no picture, 1: picture created, -1: cannot create, -2: cannot write
 *
 ***********************************************************************************/
static int handleIfCoverPicture
(
    Context *c,
    int type,
    int picsize,
    int covertype,
    const char *mime,
    TagLib::ByteVector *data,
    const char *destination_path
)
{
    audioFileInfo *info = &c->info;

    info->pic_type = 0;
    if (type == covertype)
    {
        LOGD("    front cover\n");
        if (!strcmp(mime, "image/jpeg"))
        {
            info->pic_type = 1;
            info->pic_size = picsize;
        }
        else if (!strcmp(mime, "image/png"))
        {
            info->pic_type = 2;
            info->pic_size = picsize;
        }
        else
        {
            LOGW("    skipping unsupported picture format in file \"%s\"\n", info->path);
        }
    }

    if ((info->pic_type != 0) && (destination_path != nullptr))
    {
        int ret = writePicture(c, data, info->pic_type, destination_path);
        if (ret == 0)
        {
            ret = 1;    // image successfully created
        }
        return ret;
    }
    else
    {
        return 0;   // no image created and not tried to
    }
}


/************************************************************************************
 *
 * advanced id3v1 handling
 *
 ***********************************************************************************/
static void handleId3v1(audioFileInfo *info, const TagLib::ID3v1::Tag *tag)
{
    LOGD_ID3V1("NOTICE: mp3v1 handled");

    TagLib::String str;
    const char *s;
    char buf[32];

    str = tag->album();
    s = str.toCString(true /* unicode */);
    info->tags[idAlbum] = copyNonEmptyString(s);

    str = tag->artist();
    s = str.toCString(true /* unicode */);
    info->tags[idArtist] = copyNonEmptyString(s);

    str = tag->title();
    s = str.toCString(true /* unicode */);
    info->tags[idTitle] = copyNonEmptyString(s);

    sprintf(buf, "%d", tag->track());
    info->tags[idTrack] = copyNonEmptyString(buf);

    str = tag->genre();
    s = str.toCString(true /* unicode */);
    info->tags[idGenre] = copyNonEmptyString(s);

    str = tag->comment();
    s = str.toCString(true /* unicode */);
    info->tags[idComment] = copyNonEmptyString(s);

    sprintf(buf, "%d", tag->year());
    info->tags[idYear] = copyNonEmptyString(buf);
}


/************************************************************************************
 *
 * mp3 helper
 *
 ***********************************************************************************/
static void getMp3v2Tag
(
    Context *c,
    const TagLib::ID3v2::FrameListMap *map,
    const char *key,
    const char *keyname,
    int tagno
)
{
    audioFileInfo *info = &c->info;
    (void) keyname;     // only for debugging purposes
    // Get the list of frames for a specific frame type
    TagLib::ID3v2::FrameList l = (*map)[key];
    if (!l.isEmpty())
    {
        // call front() to get only the first value. Ignore others?!?
#if 0
        // fails with corrupted string
        const char *s = l.front()->toString().toCString(true /* unicode */);
#else
        const TagLib::ID3v2::Frame * frame = l.front();
        TagLib::String str = frame->toString();                 // this object must be "alive" as long we need the text
        const char *s = str.toCString(true /* unicode */);
#endif
        LOGD("   %-40s is \"%s\"\n", keyname, s);
        if (tagno >= 0)
        {
            info->tags[tagno] = copyNonEmptyString(s);
        }
#if 0
        // debug code
        if (info->id == 11)
        {
            LOGE("getMp3Tag:   %-40s is \"%s\"\n", keyname, s);
        }
#endif
    }
    else
    {
        info->tags[tagno] = nullptr;
    }
}


/************************************************************************************
 *
 * mp3 helper
 *
 * If destination_path != null, write picture, if any
 *
 ***********************************************************************************/
static void getMp3TagWithMultipleValues
(
    Context *c,
    const TagLib::ID3v2::FrameListMap *map,
    const char *key,
    const char *keyname,
    int tagno,
    const char *destination_path
)
{
    audioFileInfo *info = &c->info;
    (void) keyname;     // only for debugging purposes
    // Get the list of frames for a specific frame type
    TagLib::ID3v2::FrameList l = (*map)[key];
    bool bIsPicture = !strcmp(key, "APIC");
    if (!l.isEmpty())
    {
        // iterate through all values for this key
        for (TagLib::ID3v2::FrameList::ConstIterator it = l.begin(); it != l.end() ; it++)
        {
            if (bIsPicture)
            {
                // strange method to cast general Frame to AttachedPictureFrame
                auto pic = dynamic_cast<TagLib::ID3v2::AttachedPictureFrame *> (*it);

                TagLib::String str_txt = pic->toString();
                TagLib::String str_mime = pic->mimeType();
                TagLib::String str_desc = pic->description();

                /*
                const char *txt  = str_txt.toCString(true);
                const char *desc = str_desc.toCString(true);
                LOGD("   picture text is \"%s\"\n", txt);
                LOGD("   picture description is \"%s\"\n", desc);
                */

                const char *mime = str_mime.toCString(true /* unicode */);
                int type = pic->type();
                TagLib::ByteVector data = pic->picture();
                int picsize = (int) data.size();
                LOGD("   picture mime type is \"%s\"\n", mime);
                LOGD("   picture type is %d\n", type);
                LOGD("   data size is %d\n", picsize);

                int ret = handleIfCoverPicture(c, type,
                                    picsize,
                                    TagLib::ID3v2::AttachedPictureFrame::FrontCover,
                                    mime,
                                    &data,
                                    destination_path);
                if (ret != 0)
                {
                    // tried to create folder image ...
                    if (ret < 0)
                    {
                        // ... but failed
                        c->nFolderImagesCreationFailed++;
                    }
                    return;
                }
            }
            else
            {
                const TagLib::ID3v2::Frame * frame = l.front();
                TagLib::String str = frame->toString();                 // this object must be "alive" as long we need the text
                const char *s = str.toCString(true /* unicode */);
                LOGD("   %-40s is \"%s\"\n", keyname, s);
                if (tagno >= 0)
                {
                    info->tags[tagno] = copyNonEmptyString(s);
                }
            }
        }
    }
    else
    {
        info->tags[tagno] = nullptr;
    }
}


/************************************************************************************
 *
 * advanced id3v2 handling
 *
 ***********************************************************************************/
static void handleId3v2(Context *c, const TagLib::ID3v2::Tag *tag)
{
    if (tag != nullptr)
    {
        const TagLib::ID3v2::FrameListMap &map = tag->frameListMap();
        audioFileInfo *info = &c->info;

        getMp3v2Tag(c, &map, "TALB", "album", idAlbum);
        getMp3v2Tag(c, &map, "TPE1", "artist (i.e. performer)", idArtist);
        getMp3v2Tag(c, &map, "TPE2", "album artist (i.e. album performer)", idAlbumArtist);
        getMp3v2Tag(c, &map, "TPE3", "conductor", idConductor);
        getMp3v2Tag(c, &map, "TIT1", "grouping (i.e. work title)", idGrouping);
        getMp3v2Tag(c, &map, "TIT2", "title (i.e. movement title)", idTitle);
        getMp3v2Tag(c, &map, "TIT3", "subtitle (e.g. opus number)", idSubtitle);
        getMp3v2Tag(c, &map, "TCOM", "composer", idComposer);
        getMp3v2Tag(c, &map, "TRCK", "track number / number of tracks", idTrack);
        getMp3v2Tag(c, &map, "TPOS", "disk number / number of disks", idDiscNo);
        //getMp3v2Tag(c, &map, "TCON", "genre", idGenre);             // WARNING: taglib in updateGenre() internally converts "(101)" to "101"!
        getMp3v2Tag(c, &map, "COMM", "comment", idComment);
        getMp3v2Tag(c, &map, "TORY", "original year", -1);
        getMp3v2Tag(c, &map, "TDRC", "year", idYear);               // WARNING: do not use TYER, as taglib internally converts v2.3 to v2.4
        getMp3v2Tag(c, &map, "MVNM", "Apple movement", idAppleMovement);
        getMp3v2Tag(c, &map, "MVIN", "Apple movement number / number of movements", idAppleMovementNo);
        getMp3v2Tag(c, &map, "GRP1", "Apple work", idAppleMp3Work);
        getMp3TagWithMultipleValues(c, &map, "APIC", "pictures", -1, nullptr);

        // special
        TagLib::String str = tag->genre();
        if (!str.isEmpty())
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
            const char *key = "(genre)";
            const char *keyname = "genre";
#pragma clang diagnostic pop
            int tagno = idGenre;

            const char *s = str.toCString(true /* unicode */);
            LOGD("   %-40s is \"%s\"\n", keyname, s);
            info->tags[tagno] = copyNonEmptyString(s);
        }

        // derived
        info->tags[idAppleWork] = info->tags[idAppleMp3Work];
    }
}


/************************************************************************************
 *
 * advanced mp3 handling
 *
 ***********************************************************************************/
void handleMp3(Context *c)
{
    audioFileInfo *info = &c->info;
    TagLib::MPEG::File f(info->path);
    TagLib::AudioProperties *props = f.audioProperties();
    handleProps(info, props);

    // Check to make sure that it has an ID3v2 tag
    TagLib::ID3v2::Tag *tag = f.ID3v2Tag();
    if ((tag != nullptr) && !tag->isEmpty())
    {
        handleId3v2(c, tag);
        info->tag_type = eTagId3v2;
    }
    else
    {
        // try ID3v1 tag
        TagLib::ID3v1::Tag *tag1 = f.ID3v1Tag();
        if (tag1 != nullptr)
        {
            handleId3v1(info, tag1);
            info->tag_type = eTagId3v1;
        }
        else
        {
            info->tag_type = eTagUnknown;
        }
    }
}


/************************************************************************************
 *
 * used for any files with id3v2 tags
 *
 ***********************************************************************************/
static void extractPictureId3v2(Context *c, const TagLib::ID3v2::Tag *tag, const char *destination_path)
{
    const TagLib::ID3v2::FrameListMap &map = tag->frameListMap();
    getMp3TagWithMultipleValues(c, &map, "APIC", "pictures", -1, destination_path);
}


/************************************************************************************
 *
 * extract picture from mp3 file
 *
 ***********************************************************************************/
void extractPictureMp3(Context *c, const char *audio_file_path, const char *destination_path)
{
    TagLib::MPEG::File f(audio_file_path);
    TagLib::ID3v2::Tag *tag = f.ID3v2Tag();
    if (tag != nullptr)
    {
        extractPictureId3v2(c, tag, destination_path);
    }
}


/************************************************************************************
 *
 * mp4 helper
 *
 ***********************************************************************************/
static void getMp4Tag
(
    Context *c,
    const TagLib::MP4::Tag *tag,
    const char *key,
    const char *keyname,
    int tagno,
    const char *destination_path
)
{
    audioFileInfo *info = &c->info;
    (void) keyname;             // for debugging purposes
    if (tag->contains(key))
    {
        TagLib::MP4::Item item = tag->item(key);
        if (!strcmp(key, "covr"))
        {
            // item list
            TagLib::MP4::CoverArtList sl = item.toCoverArtList();
            int n = (int) sl.size();
            for (int i = 0; i < n; i++)
            {
                TagLib::MP4::CoverArt coverArt = sl[i];
                TagLib::MP4::CoverArt::Format format = coverArt.format();
                TagLib::ByteVector data = coverArt.data();
                LOGD("   data size is %d\n", data.size());
                if (data.size() == 0)
                {
                    LOGW("    skipping empty picture in file \"%s\"\n", info->path);
                }
                else
                {
                    int pic_type = 0;

                    if (format == TagLib::MP4::CoverArt::JPEG)
                    {
                        LOGD("   picture format is is JPEG\n");
                        if (info != nullptr)
                        {
                            info->pic_type = 1;
                            info->pic_size = (int) data.size();
                        }
                        pic_type = 1;
                    }
                    else if (format == TagLib::MP4::CoverArt::PNG)
                    {
                        LOGD("   picture format is is PNG\n");
                        if (info != nullptr)
                        {
                            info->pic_type = 2;
                            info->pic_size = (int) data.size();
                        }
                        pic_type = 2;
                    }
                    else
                    {
                        LOGW("   picture format is is %d (unsupported, neither jpeg nor png)\n", format);
                    }

                    if ((pic_type != 0) && (destination_path != nullptr))
                    {
                        int ret = writePicture(c, &data, pic_type, destination_path);
                        if (ret < 0)
                        {
                            c->nFolderImagesCreationFailed++;
                        }
                        return;
                    }
                }
            }
        }
        else
        if (!strcmp(key, "trkn"))
        {
            TagLib::MP4::Item::IntPair pair = item.toIntPair();
            info->track_no = pair.first;
            info->no_tracks = pair.second;
            LOGD("   %-40s is %d/%d\n", keyname, pair.first, pair.second);
        }
        else
        if (!strcmp(key, "disk"))
        {
            TagLib::MP4::Item::IntPair pair = item.toIntPair();
            info->disc_no = pair.first;
            info->no_discs = pair.second;
            LOGD("   %-40s is %d/%d\n", keyname, pair.first, pair.second);
        }
        else
        {
            // string list
            TagLib::StringList sl = item.toStringList();
            int n = (int) sl.size();
            for (int i = 0; i < n; i++)
            {
                TagLib::String cs = sl[i];
                const char *s = cs.toCString(true /* unicode */);
                LOGD("   %-40s is \"%s\"\n", keyname, s);
                if ((i == 0) && (tagno >= 0))
                {
                    info->tags[tagno] = copyNonEmptyString(s);
                }
            }
        }
    }
}


/************************************************************************************
 *
 * advanced mp4 handling
 *
 ***********************************************************************************/
void handleMp4(Context *c)
{
    audioFileInfo *info = &c->info;
    TagLib::MP4::File f(info->path);
    TagLib::AudioProperties *props = f.audioProperties();
    handleProps(info, props);

    TagLib::MP4::Tag *tag = f.tag();
    if (tag != nullptr)
    {
        getMp4Tag(c, tag, "\251alb",                         "album", idAlbum, nullptr);
        getMp4Tag(c, tag, "\251ART",                         "artist (i.e. performer)", idArtist, nullptr);
        getMp4Tag(c, tag, "aART",                            "album artist (i.e. album performer)", idAlbumArtist, nullptr);
        getMp4Tag(c, tag, "----:com.apple.iTunes:CONDUCTOR", "conductor", idConductor, nullptr);
        getMp4Tag(c, tag, "\251grp",                         "grouping (i.e. work title)", idGrouping, nullptr);
        getMp4Tag(c, tag, "\251nam",                         "title (i.e. movement title)", idTitle, nullptr);
        getMp4Tag(c, tag, "----:com.apple.iTunes:SUBTITLE",  "subtitle (e.g. opus number)", idSubtitle, nullptr);
        getMp4Tag(c, tag, "\251wrt",                         "composer", idComposer, nullptr);
        getMp4Tag(c, tag, "trkn",                            "track number / number of tracks", idTrack, nullptr);
        getMp4Tag(c, tag, "disk",                            "disk number / number of disks", idDiscNo, nullptr);
        getMp4Tag(c, tag, "\251gen",                         "genre", idGenre, nullptr);
        getMp4Tag(c, tag, "\251cmt",                         "comment", idComment, nullptr);
        getMp4Tag(c, tag, "ORIGINALDATE",                    "original year", -1, nullptr);
        getMp4Tag(c, tag, "\251day",                         "year", idYear, nullptr);
        getMp4Tag(c, tag, "\251mvn",                         "Apple movement", idAppleMovement, nullptr);
        getMp4Tag(c, tag, "\251mvi",                         "Apple movement number", idAppleMovementNo, nullptr);
        getMp4Tag(c, tag, "\251mvc",                         "Apple number of movements", idAppleMovementTotal, nullptr);
        getMp4Tag(c, tag, "\251wrk",                         "Apple work", idAppleMp4Work, nullptr);
        getMp4Tag(c, tag, "covr",                            "pictures", -1, nullptr);

        // derived
        info->tags[idAppleWork] = info->tags[idAppleMp4Work];
    }

    info->tag_type = eTagMp4;
}


/************************************************************************************
 *
 * extract picture from mp4 file
 *
 ***********************************************************************************/
void extractPictureMp4(Context *c, const char *audio_file_path, const char *destination_path)
{
    TagLib::MP4::File f(audio_file_path);

    TagLib::MP4::Tag *tag = f.tag();
    if (tag != nullptr)
    {
        getMp4Tag(c, tag, "covr", "pictures", -1, destination_path);
    }
}


/************************************************************************************
 *
 * flac helper
 *
 ***********************************************************************************/
static void getOggTag
(
    audioFileInfo *info,
    const TagLib::Ogg::FieldListMap *map,
    const char *key,
    const char *keyname,
    int tagno
)
{
    (void) keyname;     // only for debugging purposes
    if (map->contains(key))
    {
        TagLib::StringList sl = (*map)[key];
        int n = (int) sl.size();
        for (int i = 0; i < n; i++)
        {
            TagLib::String cs = sl[i];
            const char *s = cs.toCString(true /* unicode */);
            LOGD("   %-40s is \"%s\"\n", keyname, s);
            if ((i == 0) && (tagno >= 0))
            {
                info->tags[tagno] = copyNonEmptyString(s);
            }
        }
    }
}


/************************************************************************************
 *
 * advanced ogg handling (vorbis and flac)
 *
 ***********************************************************************************/
static void handleOgg(audioFileInfo *info, TagLib::Ogg::XiphComment *tag)
{
    const TagLib::Ogg::FieldListMap &map = tag->fieldListMap();

    getOggTag(info, &map, "ALBUM", "album", idAlbum);
    getOggTag(info, &map, "ARTIST", "artist (i.e. performer)", idArtist);
    getOggTag(info, &map, "ALBUMARTIST", "album artist (i.e. album performer)", idAlbumArtist);
    getOggTag(info, &map, "CONDUCTOR", "conductor", idConductor);
    getOggTag(info, &map, "GROUPING", "grouping (i.e. work title)", idGrouping);
    getOggTag(info, &map, "TITLE", "title (i.e. movement title)", idTitle);
    getOggTag(info, &map, "SUBTITLE", "subtitle (e.g. opus number)", idSubtitle);
    getOggTag(info, &map, "COMPOSER", "composer", idComposer);
    getOggTag(info, &map, "TRACKNUMBER", "track number / number of tracks", idTrack);
    getOggTag(info, &map, "DISCNUMBER", "disk number / number of disks", idDiscNo);
    getOggTag(info, &map, "GENRE", "genre", idGenre);
    getOggTag(info, &map, "COMMENT", "comment", idComment);
    getOggTag(info, &map, "ORIGINALDATE", "original year", -1);
    getOggTag(info, &map, "DATE", "year", idYear);
}


/************************************************************************************
 *
 * handle flac pictures
 *
 ***********************************************************************************/
void handlePictureFlac(Context *c, const char *audio_file_path, const char *destination_path)
{
    LOGI("looking for pictures in file \"%s\"\n", audio_file_path);
    audioFileInfo *info = &c->info;
    TagLib::FLAC::File f(audio_file_path);
    const TagLib::List<TagLib::FLAC::Picture*>& picList = f.pictureList();
    int size = (int) picList.size();
    bool bDone = false;

    for (int i = 0; i < size; i++)
    {
        TagLib::FLAC::Picture *pic = picList[i];
        int type = pic->type();
        TagLib::String str_mime = pic->mimeType();
        TagLib::ByteVector data = pic->data();
        int picsize = (int) data.size();

        const char *mime = str_mime.toCString(true /* unicode */);
        const char *stype;
        switch(type)
        {
            case TagLib::FLAC::Picture::FileIcon    : stype = "FileIcon   "; break;
            case TagLib::FLAC::Picture::FrontCover  : stype = "FrontCover "; break;
            case TagLib::FLAC::Picture::BackCover   : stype = "BackCover  "; break;
            case TagLib::FLAC::Picture::LeafletPage : stype = "LeafletPage"; break;
            case TagLib::FLAC::Picture::Media       : stype = "Media      "; break;
            case TagLib::FLAC::Picture::LeadArtist  : stype = "LeadArtist "; break;
            case TagLib::FLAC::Picture::Artist      : stype = "Artist     "; break;
            case TagLib::FLAC::Picture::Conductor   : stype = "Conductor  "; break;
            case TagLib::FLAC::Picture::Band        : stype = "Band       "; break;
            case TagLib::FLAC::Picture::Composer    : stype = "Composer   "; break;
            case TagLib::FLAC::Picture::Lyricist    : stype = "Lyricist   "; break;
            default: stype = "other";
        }
        LOGI("picture found in flac file, type = %s (%d), mime type = %s, size = %d x %d px\n", stype, type, mime, pic->width(), pic->height());

        if (picsize == 0)
        {
            LOGW("    skipping empty picture in file \"%s\"\n", info->path);
        }
        else
        if (!bDone)
        {
            int ret = handleIfCoverPicture(c, type, picsize, TagLib::FLAC::Picture::FrontCover, mime, &data, destination_path);
            if (ret != 0)
            {
                // tried to create folder image ...
                if (ret < 0)
                {
                    // ... but failed
                    c->nFolderImagesCreationFailed++;
                }
                bDone = true;   // no further attempts
            }
        }
    }
}


/************************************************************************************
 *
 * advanced flac handling
 *
 ***********************************************************************************/
void handleFlac(Context *c)
{
    audioFileInfo *info = &c->info;
    TagLib::FLAC::File f(info->path);
    TagLib::AudioProperties *props = f.audioProperties();
    handleProps(info, props);

    if (f.hasXiphComment())
    {
        TagLib::Ogg::XiphComment *tag = f.xiphComment();
        if (tag != nullptr)
        {
            handleOgg(info, tag);
            info->tag_type = eTagOgg;
        }
    }
    if (f.hasID3v1Tag())
    {
        TagLib::ID3v1::Tag *tag = f.ID3v1Tag();
        if (tag != nullptr)
        {
            handleId3v1(info, tag);
            info->tag_type = eTagId3v1;
        }
    }
    if (f.hasID3v2Tag())
    {
        TagLib::ID3v2::Tag *tag = f.ID3v2Tag();
        if (tag != nullptr)
        {
            handleId3v2(c, tag);
            info->tag_type = eTagId3v2;
        }
    }

    if (info->pic_type == 0)
    {
        // no picture found so far. Search for one, but do not extract it
        handlePictureFlac(c, c->info.path, nullptr);
    }
}


/************************************************************************************
 *
 * extract picture from flac file
 *
 ***********************************************************************************/
void extractPictureFlac(Context *c, const char *audio_file_path, const char *destination_path)
{
    LOGI("looking for pictures in file \"%s\"\n", audio_file_path);
    audioFileInfo *info = &c->info;
    TagLib::FLAC::File f(audio_file_path);
    info->pic_type = 0;

    if (f.hasXiphComment())
    {
        /*
        TagLib::Ogg::XiphComment *tag = f.xiphComment();
        if (tag != nullptr)
        {
            handleOgg(info, tag);
        }
         */
    }
    if (f.hasID3v1Tag())
    {
        /*
        TagLib::ID3v1::Tag *tag = f.ID3v1Tag();
        if (tag != nullptr)
        {

        }
        */
    }
    if (f.hasID3v2Tag())
    {
        TagLib::ID3v2::Tag *tag = f.ID3v2Tag();
        if (tag != nullptr)
        {
            extractPictureId3v2(c, tag, destination_path);
        }
    }

    if (info->pic_type == 0)
    {
        // no picture found so far. Search for one and extract it
        handlePictureFlac(c, audio_file_path, destination_path);
    }
}


/************************************************************************************
 *
 * advanced ogg handling
 *
 ***********************************************************************************/
void handleOggVorbis(Context *c)
{
    audioFileInfo *info = &c->info;
    TagLib::Ogg::Vorbis::File f(info->path);
    TagLib::AudioProperties *props = f.audioProperties();
    handleProps(info, props);

    TagLib::Ogg::XiphComment *tag = f.tag();
    if (tag != nullptr)
    {
        handleOgg(info, tag);
        info->tag_type = eTagOgg;
    }
}


/************************************************************************************
 *
 * extract picture from ogg file
 *
 ***********************************************************************************/
void extractPictureOggVorbis(Context *c, const char *audio_file_path, const char *destination_path)
{
    // ogg does not support embedded images
    (void) c;
    (void) audio_file_path;
    (void) destination_path;
}
