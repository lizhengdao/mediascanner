/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma ide diagnostic ignored "EmptyDeclOrStmt"

//#include <string>
#include <dirent.h>
#include <cerrno>
//#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>

// Android
#include <android/log.h>

// advanced taglib interface
#include <mpeg/id3v2/id3v2tag.h>
#include <mpeg/id3v1/id3v1tag.h>
#include <mpeg/id3v2/frames/attachedpictureframe.h>
#include "mpegfile.h"
#include <mp4/mp4file.h>
#include <ogg/flac/oggflacfile.h>
#include <ogg/vorbis/vorbisfile.h>
#include <flac/flacfile.h>

#include "native-lib-utils.h"
#include "native-lib.h"
#include "native-lib-db.h"


/******************************************************************************
 *
 * SqLite callback
 *
 * the first parameter is used for C++ as "this" pointer
 *
 * If this function returns a value other than 0, the sqlite3_exec()
 * call will stop with SQLITE_ABORT.
 *
 *****************************************************************************/
static int callback(void *p, int argc, char **argv, char **azColName)
{
    auto *c = (Context *) p;
    int i;
    for (i = 0; i < argc; i++)
    {
        LOGI("SqLite cb: %s = %s\n", azColName[i], argv[i] ? argv[i] : "(NULL)");
        if (i == 0)
        {
            c->sql_last_qres0 = argv[i] ? atoll(argv[i]) : 0; // NOLINT(cert-err34-c)
        }
    }
    LOGI("----------------------------------------\n");
    return 0;
}


/******************************************************************************
 *
 * create information table
 *
 *****************************************************************************/
int createAndSetSqlTableInfo(Context *c)
{
    const char *sql = "CREATE TABLE IF NOT EXISTS " INFO_TABLE_NAME "("  \
        "DB_VERSION"         " INT" \
        "); INSERT INTO " INFO_TABLE_NAME " (DB_VERSION) VALUES (" xstr(DB_VERSION) ");";

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    LOGD("SqLite call: %s\n", sql);
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGD("Table created successfully\n");
        return 0;
    }
}


/******************************************************************************
 *
 * get DB_VERSION value from info table
 *
 *****************************************************************************/
int getDbVersionFromSqlTableInfo(Context *c)
{
    const char *sql = "SELECT DB_VERSION FROM " INFO_TABLE_NAME ";";

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGI("value requested successfully\n");
        return (int) c->sql_last_qres0;
    }
}


/******************************************************************************
 *
 * create files table
 *
 *****************************************************************************/
int createSqlTableMusicFiles(Context *c)
{
    const char *sql = "CREATE TABLE IF NOT EXISTS " FILES_TABLE_NAME "("  \
        FILES_TABLE_TCOL_ID           " INT PRIMARY KEY NOT NULL," \
        FILES_TABLE_TCOL_TRACK_NO     " INT," \
        FILES_TABLE_TCOL_TITLE        " TEXT," \
        FILES_TABLE_TCOL_ALBUM        " TEXT," \
        FILES_TABLE_TCOL_ALBUM_ID     " INT," \
        FILES_TABLE_TCOL_DURATION     " INT," \
        FILES_TABLE_TCOL_GROUPING     " TEXT," \
        FILES_TABLE_TCOL_SUBTITLE     " TEXT," \
        FILES_TABLE_TCOL_COMPOSER     " TEXT," \
        FILES_TABLE_TCOL_PERFORMER    " TEXT," \
        FILES_TABLE_TCOL_ALBUM_ARTIST " TEXT," \
        FILES_TABLE_TCOL_CONDUCTOR    " TEXT," \
        FILES_TABLE_TCOL_GENRE        " TEXT," \
        FILES_TABLE_TCOL_YEAR         " INT," \
        FILES_TABLE_TCOL_PATH         " TEXT NOT NULL UNIQUE ON CONFLICT ROLLBACK," \
        FILES_TABLE_TCOL_PIC_TYPE     " INT," \
        FILES_TABLE_TCOL_PIC_SIZE     " INT," \
        FILES_TABLE_TCOL_TAG_TYPE     " INT," \
        FILES_TABLE_TCOL_MTIME        " INT" \
        ");";

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGD("Table created successfully\n");
        return 0;
    }
}


/******************************************************************************
 *
 * create albums table
 *
 *****************************************************************************/
int createSqlTableAlbums(Context *c)
{
    const char *sql = "CREATE TABLE IF NOT EXISTS " ALBUMS_TABLE_NAME "("  \
        ALBUMS_TABLE_TCOL_ID         " INT PRIMARY KEY NOT NULL," \
        ALBUMS_TABLE_TCOL_NAME       " TEXT," \
        ALBUMS_TABLE_TCOL_ARTIST     " TEXT," \
        ALBUMS_TABLE_TCOL_COMPOSER   " TEXT," \
        ALBUMS_TABLE_TCOL_PERFORMER  " TEXT," \
        ALBUMS_TABLE_TCOL_CONDUCTOR  " TEXT," \
        ALBUMS_TABLE_TCOL_GENRE      " TEXT," \
        ALBUMS_TABLE_TCOL_FIRST_YEAR " INT," \
        ALBUMS_TABLE_TCOL_LAST_YEAR  " INT," \
        ALBUMS_TABLE_TCOL_NO_TRACKS  " INT," \
        ALBUMS_TABLE_TCOL_DURATION   " INT," \
        ALBUMS_TABLE_TCOL_PATH       " TEXT," \
        ALBUMS_TABLE_TCOL_PICTURE    " TEXT" \
        ");";

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGD("Table created successfully\n");
        return 0;
    }
}


/******************************************************************************
 *
 * truncate table, i.e. delete all rows
 *
 *****************************************************************************/
int truncateSqlTable(Context *c, const char *table_name)
{
    const char * tsql = "DELETE FROM %s;";
    char sql[MAXSQL];
    int n;
    n = snprintf(sql, MAXSQL, tsql, table_name);
    if ((n < 0) || (n >= MAXSQL))
    {
        LOGE("SQL command length overflow.");
        return -2;
    }

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL command failed : \"%s\"", sql);
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGI("Table truncated successfully\n");
        return 0;
    }
}


/******************************************************************************
 *
 * delete single entry from table, given by id
 *
 *****************************************************************************/
int removeIdFromSqlTable(Context *c, const char *table_name, int64_t id)
{
    const char * tsql = "DELETE FROM %s WHERE " FILES_TABLE_TCOL_ID " = %" PRId64 ";";
    char sql[MAXSQL];
    int n;
    n = snprintf(sql, MAXSQL, tsql, table_name, id);
    if ((n < 0) || (n >= MAXSQL))
    {
        LOGE("SQL command length overflow.");
        return -2;
    }

    /* Execute SQL statement */
    LOGD("SqLite call: %s\n", sql);
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL command failed : \"%s\"", sql);
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGI("Entry %" PRId64 " removed from %s\n", id, table_name);
        return 0;
    }
}


#if 0
/******************************************************************************
 *
 * delete single entry from table, given by album and album artist
 *
 * Note that album name and album artist may include quotation characters, so that
 * we cannot simply build an SQL command in text form without doubling the quotes.
 *
 * As a workaround, we do not use sqlite3_exec(), but the more sophisticated
 * combination sqlite3_prepare_v2()/sqlite3_bind_text()/sqlite3_step()/sqlite3_finalize()
 *
 *****************************************************************************/
int removeFromAlbumsSqlTable(Context *c, const char *album_name, const char *album_artist)
{
    const char * tsql1 = "DELETE FROM " ALBUMS_TABLE_NAME " WHERE " ALBUMS_TABLE_TCOL_NAME " = ? AND " ALBUMS_TABLE_TCOL_ARTIST " = ?;";
    const char * tsql2 = "DELETE FROM " ALBUMS_TABLE_NAME " WHERE " ALBUMS_TABLE_TCOL_NAME " = ? AND " ALBUMS_TABLE_TCOL_ARTIST " IS NULL;";
    const char *sql;

    bool bIsArtist = !strZeroOrEmpty(album_artist);
    sql = (bIsArtist) ? tsql1 : tsql2;

    sqlite3_stmt *stmt;
    LOGD("DB: execute SQL command: %s\n", sql);
    int rc = sqlite3_prepare_v2(c->db, sql, -1, &stmt, nullptr);
    if (rc)
    {
        LOGE("DB: Can't prepare: %s\n", sqlite3_errmsg(c->db));
        LOGD("DB: failing SQL command: %s\n", sql);
        return -2;
    }

    rc = sqlite3_bind_text(stmt, 1, album_name, -1, SQLITE_TRANSIENT);
    LOGD("DB:  with parameter #1: %s\n", album_name);
    if ((rc == SQLITE_OK) && (bIsArtist))
    {
        rc = sqlite3_bind_text(stmt, 2, album_artist, -1, SQLITE_TRANSIENT);
        LOGD("DB:  with parameter #2: %s\n", album_artist);
    }

    if (rc == SQLITE_OK)
    {
        /* Execute SQL statement */
        rc = sqlite3_step(stmt);

        if (rc != SQLITE_DONE)
        {
            LOGE("SQL command failed : \"%s\"", sql);
            const char *zErrMsg = sqlite3_errmsg(c->db);
            LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        }
        else
        {
            LOGD("Row deleted successfully\n");
        }

        rc = sqlite3_finalize(stmt);
        if (rc)
        {
            LOGE("DB: Can't finalize: %s\n", sqlite3_errmsg(c->db));
        }
    }

    return 0;
}
#endif


/******************************************************************************
 *
 * remove table, also deleting all rows
 *
 *****************************************************************************/
int removeSqlTable(Context *c, const char *table_name)
{
    const char * tsql = "DROP TABLE IF EXISTS %s;";
    char sql[MAXSQL];
    int n;
    n = snprintf(sql, MAXSQL, tsql, table_name);
    if ((n < 0) || (n >= MAXSQL))
    {
        LOGE("SQL command length overflow.");
        return -2;
    }

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    LOGI("SqLite call: %s\n", sql);
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL command failed : \"%s\"", sql);
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGI("Table removed successfully\n");
        return 0;
    }
}


/******************************************************************************
 *
 * get number of rows in table
 *
 *****************************************************************************/
int getNumRowsOfSqlTable(Context *c, const char *table_name)
{
    const char *tsql = "SELECT Count(*) FROM %s;";
    char sql[MAXSQL];
    int n;
    n = snprintf(sql, MAXSQL, tsql, table_name);
    if ((n < 0) || (n >= MAXSQL))
    {
        LOGE("SQL command length overflow.");
        return -2;
    }

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGI("Table requested successfully\n");
        return (int) c->sql_last_qres0;
    }
}


/******************************************************************************
 *
 * get max id of table
 *
 *****************************************************************************/
int64_t getMaxAlbumId(Context *c)
{
    const char *sql = "SELECT Max(" ALBUMS_TABLE_TCOL_ID ") FROM " ALBUMS_TABLE_NAME ";";
    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    c->sql_last_qres0 = 0;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGI("Table requested successfully\n");
        return c->sql_last_qres0;
    }
}


/******************************************************************************
 *
 * get max id of table
 *
 *****************************************************************************/
int64_t getMaxFileId(Context *c)
{
    const char *sql = "SELECT Max(" FILES_TABLE_TCOL_ID ") FROM " FILES_TABLE_NAME ";";
    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    c->sql_last_qres0 = 0;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGI("Table requested successfully\n");
        return c->sql_last_qres0;
    }
}


/******************************************************************************
 *
 * prepare INSERT SQL command for file table
 *
 *****************************************************************************/
void prepareSqlInsertToFileTable(Context *c)
{
    const char *sql = "INSERT INTO " FILES_TABLE_NAME " ("
                      FILES_TABLE_TCOL_ID           "," \
                      FILES_TABLE_TCOL_TRACK_NO     "," \
                      FILES_TABLE_TCOL_TITLE        "," \
                      FILES_TABLE_TCOL_ALBUM        "," \
                      FILES_TABLE_TCOL_ALBUM_ID     "," \
                      FILES_TABLE_TCOL_DURATION     "," \
                      FILES_TABLE_TCOL_GROUPING     "," \
                      FILES_TABLE_TCOL_SUBTITLE     "," \
                      FILES_TABLE_TCOL_COMPOSER     "," \
                      FILES_TABLE_TCOL_PERFORMER    "," \
                      FILES_TABLE_TCOL_ALBUM_ARTIST "," \
                      FILES_TABLE_TCOL_CONDUCTOR    "," \
                      FILES_TABLE_TCOL_GENRE        "," \
                      FILES_TABLE_TCOL_YEAR         "," \
                      FILES_TABLE_TCOL_PATH         "," \
                      FILES_TABLE_TCOL_PIC_TYPE     "," \
                      FILES_TABLE_TCOL_PIC_SIZE     "," \
                      FILES_TABLE_TCOL_TAG_TYPE     "," \
                      FILES_TABLE_TCOL_MTIME
                       ") "\
                       "VALUES (?,?,?,?," "?,?,?,?," "?,?,?,?" ",?,?,?,?" ",?,?,?);";
    int rc = sqlite3_prepare_v2(c->db, sql, -1, &c->ins_stmt, nullptr);
    if (rc)
    {
        LOGE("DB: Can't prepare: %s\nCommand was \"%s\"", sqlite3_errmsg(c->db), sql);
    }
}


/******************************************************************************
 *
 * prepare INSERT SQL command for album table
 *
 *****************************************************************************/
static void prepareSqlInsertToAlbumTable(Context *c, sqlite3_stmt **ins_stmt)
{
    int rc = sqlite3_prepare_v2(c->db, "INSERT INTO " ALBUMS_TABLE_NAME " ("
                           ALBUMS_TABLE_TCOL_ID         "," \
                           ALBUMS_TABLE_TCOL_NAME       "," \
                           ALBUMS_TABLE_TCOL_ARTIST     "," \
                           ALBUMS_TABLE_TCOL_COMPOSER   "," \
                           ALBUMS_TABLE_TCOL_PERFORMER  "," \
                           ALBUMS_TABLE_TCOL_CONDUCTOR  "," \
                           ALBUMS_TABLE_TCOL_GENRE      "," \
                           ALBUMS_TABLE_TCOL_FIRST_YEAR "," \
                           ALBUMS_TABLE_TCOL_LAST_YEAR  "," \
                           ALBUMS_TABLE_TCOL_NO_TRACKS  "," \
                           ALBUMS_TABLE_TCOL_DURATION   "," \
                           ALBUMS_TABLE_TCOL_PATH       "," \
                           ALBUMS_TABLE_TCOL_PICTURE        \
                           ") "\
                           "VALUES (?,?,?,?," "?,?,?,?," "?,?,?,?,?);",
                                -1, ins_stmt, nullptr);
    if (rc)
    {
        LOGE("DB: Can't prepare: %s\n", sqlite3_errmsg(c->db));
    }
}


#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-avoid-goto"
/******************************************************************************
 *
 * execute INSERT SQL command for file table
 *
 *****************************************************************************/
int executeSqlInsertToFileTable(Context *c, const audioFileInfo *t)
{
    int rc;
    rc = sqlite3_bind_int64(c->ins_stmt, FILES_TABLE_COL_ID           + 1, t->id);                                 if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (c->ins_stmt, FILES_TABLE_COL_TRACK_NO     + 1, t->track_disc_no);                      if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_TITLE        + 1, t->title,        -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_ALBUM        + 1, t->album,        -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int64(c->ins_stmt, FILES_TABLE_COL_ALBUM_ID     + 1, t->album_id);                           if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (c->ins_stmt, FILES_TABLE_COL_DURATION     + 1, t->duration);                           if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_GROUPING     + 1, t->grouping,     -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_SUBTITLE     + 1, t->subtitle,     -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_COMPOSER     + 1, t->composer,     -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_PERFORMER    + 1, t->performer,    -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_ALBUM_ARTIST + 1, t->album_artist, -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_CONDUCTOR    + 1, t->conductor,    -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_GENRE        + 1, t->genre,        -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (c->ins_stmt, FILES_TABLE_COL_YEAR         + 1, t->year);                               if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (c->ins_stmt, FILES_TABLE_COL_PATH         + 1, t->path,         -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (c->ins_stmt, FILES_TABLE_COL_PIC_TYPE     + 1, t->pic_type);                           if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (c->ins_stmt, FILES_TABLE_COL_PIC_SIZE     + 1, t->pic_size);                           if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (c->ins_stmt, FILES_TABLE_COL_TAG_TYPE     + 1, t->tag_type);                           if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int64(c->ins_stmt, FILES_TABLE_COL_MTIME        + 1, t->mtime);                              if (rc != SQLITE_OK) goto err;

    LOGD("DB: Insert id %" PRId64 " to files table\n", t->id);
    rc = sqlite3_step(c->ins_stmt);
    sqlite3_reset(c->ins_stmt);
    err:
    if (rc == SQLITE_DONE)
    {
        c->nAudioFilesFound++;
        doProgressCallback(c, eResultProgress);
        return 0;
    }

    //
    // failure handling
    //

    const char *errmsg = sqlite3_errmsg(c->db);
    if (rc == SQLITE_CONSTRAINT)
    {
        const char *s = strstr(errmsg, FILES_TABLE_NAME "." FILES_TABLE_TCOL_PATH);
        if (s != nullptr)
        {
            LOGW("DB: %s (%d): PATH %s already exists, skipping\n", errmsg, rc, t->path);
            rc = 0;
        }
        else
        {
            LOGE("DB: %s (%d): _id %"
                         PRId64
                         " exists\n", errmsg, rc, t->id);
            rc = -1;
        }
    }
    else
    {
        LOGE("DB: Can't step when inserting %" PRId64 " into files table: %s (%d)\n", t->id, errmsg, rc);
        rc = -2;
    }

    return  rc;
}
#pragma clang diagnostic pop


/******************************************************************************
 *
 * replace empty string with null
 *
 *****************************************************************************/
inline int sqlite3_bind_textZ(sqlite3_stmt *stmt, int n, const char *t)
{
    if (strZeroOrEmpty(t))
    {
        t = nullptr;
    }
    return sqlite3_bind_text(stmt, n, t, -1, SQLITE_TRANSIENT);
}


#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-avoid-goto"
/******************************************************************************
 *
 * execute INSERT SQL command for album table
 *
 *****************************************************************************/
static void executeSqlInsertToAlbumTable
(
    Context *c,
    const audioAlbumInfo *t,
    sqlite3_stmt *ins_stmt
)
{
    int rc;
    rc = sqlite3_bind_int64(ins_stmt, ALBUMS_TABLE_COL_ID         + 1, t->album_id);                              if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (ins_stmt, ALBUMS_TABLE_COL_NAME       + 1, t->album_name,      -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_textZ(ins_stmt, ALBUMS_TABLE_COL_ARTIST     + 1, t->album_artist);                          if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_textZ(ins_stmt, ALBUMS_TABLE_COL_COMPOSER   + 1, t->album_composer);                        if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_textZ(ins_stmt, ALBUMS_TABLE_COL_PERFORMER  + 1, t->album_performer);                       if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_textZ(ins_stmt, ALBUMS_TABLE_COL_CONDUCTOR  + 1, t->album_conductor);                       if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_textZ(ins_stmt, ALBUMS_TABLE_COL_GENRE      + 1, t->album_genre);                           if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (ins_stmt, ALBUMS_TABLE_COL_FIRST_YEAR + 1, t->album_first_year);                      if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (ins_stmt, ALBUMS_TABLE_COL_LAST_YEAR  + 1, t->album_last_year);                       if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (ins_stmt, ALBUMS_TABLE_COL_NO_TRACKS  + 1, t->album_no_tracks);                       if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_int  (ins_stmt, ALBUMS_TABLE_COL_DURATION   + 1, t->album_duration);                        if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_text (ins_stmt, ALBUMS_TABLE_COL_PATH       + 1, t->album_path,      -1, SQLITE_TRANSIENT); if (rc != SQLITE_OK) goto err;
    rc = sqlite3_bind_textZ(ins_stmt, ALBUMS_TABLE_COL_PICTURE    + 1, t->album_picture);                         if (rc != SQLITE_OK) goto err;

    LOGD("DB: Insert id %" PRId64 " to albums table\n", t->album_id);
    rc = sqlite3_step(ins_stmt);
    sqlite3_reset(ins_stmt);
    err:
    if (rc == SQLITE_DONE)
    {
        doProgressCallback(c, eResultProgress);
        c->nAlbumsFound++;
    }
    else
    if (rc == SQLITE_CONSTRAINT)
    {
        LOGE("DB: Can't step: %s (%d): _id %" PRId64 " exists\n", sqlite3_errmsg(c->db), rc, t->album_id);
    }
    else
    {
        LOGE("DB: Can't step while inserting album %" PRId64 ": %s (%d)\n", t->album_id, sqlite3_errmsg(c->db), rc);
    }

    LOGD("DB: created album id %" PRId64 "\n", t->album_id);
}
#pragma clang diagnostic pop


/******************************************************************************
 *
 * free SQL command ("finalize")
 *
 *****************************************************************************/
void freeSqlInsert(Context *c)
{
    if (c->ins_stmt != nullptr)
    {
        int rc = sqlite3_finalize(c->ins_stmt);
        if (rc)
        {
            LOGE("DB: Can't finalize: %s\n", sqlite3_errmsg(c->db));
        }
        c->ins_stmt = nullptr;
    }
}


/******************************************************************************
 *
 * get all rows of files table
 *
 *****************************************************************************/
void runSqlQueryAllFiles(Context *c)
{
    const char *sql = "SELECT * FROM " FILES_TABLE_NAME ";";

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
    }
    else
    {
        LOGI("Table requested successfully\n");
    }
}


/******************************************************************************
 *
 * get all rows of albums table
 *
 *****************************************************************************/
void runSqlQueryAllAlbums(Context *c)
{
    const char *sql = "SELECT * FROM " ALBUMS_TABLE_NAME ";";

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
    }
    else
    {
        LOGI("Table requested successfully\n");
    }
}


/******************************************************************************
 *
 * update a single picture path for an album id in albums table
 *
 *****************************************************************************/
static int executeSqlUpdatePicturePathInAlbumsTable
(
    Context *c,
    int64_t album_id,
    const char *picture_path
)
{
    const char * tsql1 = "UPDATE " ALBUMS_TABLE_NAME " SET " ALBUMS_TABLE_TCOL_PICTURE " = NULL WHERE " ALBUMS_TABLE_TCOL_ID " = %" PRId64 ";";
    const char * tsql2 = "UPDATE " ALBUMS_TABLE_NAME " SET " ALBUMS_TABLE_TCOL_PICTURE " = \"%s\" WHERE " ALBUMS_TABLE_TCOL_ID " = %" PRId64 ";";
    char sql[MAXSQL];
    int n;
    if (picture_path == nullptr)
    {
        n = snprintf(sql, MAXSQL, tsql1, album_id);
    }
    else
    {
        n = snprintf(sql, MAXSQL, tsql2, picture_path, album_id);
    }
    if ((n < 0) || (n >= MAXSQL))
    {
        LOGE("SQL command length overflow.");
        return -2;
    }
    LOGI_ALBUM("DB: execute SQL command: %s\n", sql);

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
        return -1;
    }
    else
    {
        LOGI_ALBUM("Table updated successfully\n");
        if (picture_path == nullptr)
        {
            (c->nAlbumPicturesRemoved)++;
        }
        else
        {
            (c->nAlbumPicturesAdded)++;
        }
        return (int) c->sql_last_qres0;
    }
}


/******************************************************************************
 *
 * SqLite callback
 *
 * the first parameter is used for C++ as "this" pointer
 *
 * If this function returns a value other than 0, the sqlite3_exec()
 * call will stop with SQLITE_ABORT.
 *
 * argv[0] = album id
 * argv[1] = album path
 * argv[2] = album picture or NULL
 *
 *****************************************************************************/
static int callback_update_picture(void *p, int argc, char **argv, char **azColName)
{
    auto *c = (Context *) p;
    (void) azColName;

    // sanity checks
    assert(argc == 3);
    assert(argv[0] != nullptr);
    assert(argv[1] != nullptr);
    int64_t album_id = atoll(argv[0]); // NOLINT(cert-err34-c)
    const char *album_path = argv[1];
    const char *picture_path = argv[2];

#if 0
    // test code
    static int cnt = 0;
    if (cnt & 1U)
    {
        // remove every other album picture path
        (void) executeSqlUpdatePicturePathInAlbumsTable(c, album_id, nullptr);
    }
    cnt++;
    return 0;
#endif

    //
    // check if picture path is valid and picture exists
    //

    if (picture_path != nullptr)
    {
        struct stat statbuf; // NOLINT(cppcoreguidelines-pro-type-member-init,hicpp-member-init)
        int res = stat(picture_path, &statbuf);
        // directory entry must exist and must be a regular file
        if ((res == 0) && ((statbuf.st_mode & S_IFMT) == S_IFREG)) // NOLINT(hicpp-signed-bitwise)
        {
            // no action required
            return 0;
        }

        LOGI_ALBUM("picture path for album id %" PRIi64 " has become invalid: %s\n", album_id, picture_path);
    }

    //
    // picture is invalid or did not exist. Look for one.
    //

    audioAlbumInfo album_info;
    memset(&album_info, 0, sizeof(album_info));
    strcpy(album_info.album_path, album_path);
    strcat(album_info.album_path, "/");
    checkAlbumPicture(&album_info, album_info.album_path, strlen(album_info.album_path));
    scaleAlbumPicture(c, &album_info);
    const char *picture_path_new = (album_info.album_picture[0]) ? album_info.album_picture : nullptr;
    if ((picture_path == nullptr) && (picture_path_new == nullptr))
    {
        LOGI_ALBUM("still no picture for album id %" PRIi64 " in path %s\n", album_id, album_path);
        // no action required
        return 0;
    }

    //
    // picture has either become valid or invalid. Database update required.
    //

    if (picture_path_new != nullptr)
    {
        LOGI_ALBUM("picture for album id %" PRIi64 " found: %s\n", album_id, picture_path_new);
    }
    else
    {
        LOGI_ALBUM("picture for album id %" PRIi64 " lost: %s\n", album_id, picture_path);
    }

    //TODO: error ignored here, because we will continue with other albums
    (void) executeSqlUpdatePicturePathInAlbumsTable(c, album_id, picture_path_new);

    return 0;
}


/******************************************************************************
 *
 * get all rows of albums table and update the pictures, if necessary
 *
 *****************************************************************************/
int runSqlUpdateAllAlbumPictures(Context *c)
{
    const char *sql = "SELECT " ALBUMS_TABLE_TCOL_ID "," ALBUMS_TABLE_TCOL_PATH "," ALBUMS_TABLE_TCOL_PICTURE " FROM " ALBUMS_TABLE_NAME ";";

    /* Execute SQL statement */
    char *zErrMsg = nullptr;
    int rc = sqlite3_exec(c->db, sql, callback_update_picture, c, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        sqlite3_free(zErrMsg);
    }
    else
    {
        LOGI_ALBUM("Table requested successfully\n");
    }
    return rc;
}


/******************************************************************************
 *
 * write album id to audio files table
 *
 * Note that album name and album artist may include quotation characters, so that
 * we cannot simply build an SQL command in text form without doubling the quotes.
 *
 * As a workaround, we do not use sqlite3_exec(), but the more sophisticated
 * combination sqlite3_prepare_v2()/sqlite3_bind_text()/sqlite3_step()/sqlite3_finalize()
 *
 *****************************************************************************/
void executeSqlUpdateAlbumIdsInFilesTable
(
    Context *c,
    audioAlbumInfo *album_info
)
{
    const char * tsql1 = "UPDATE " FILES_TABLE_NAME " SET " FILES_TABLE_TCOL_ALBUM_ID " = %" PRId64 " WHERE " FILES_TABLE_TCOL_ALBUM " = ? AND " FILES_TABLE_TCOL_ALBUM_ARTIST " = ?;";
    const char * tsql2 = "UPDATE " FILES_TABLE_NAME " SET " FILES_TABLE_TCOL_ALBUM_ID " = %" PRId64 " WHERE " FILES_TABLE_TCOL_ALBUM " = ? AND " FILES_TABLE_TCOL_ALBUM_ARTIST " IS NULL;";
    char sql[MAXSQL];

    bool bIsArtist = (album_info->album_artist[0] != '\0');
    int n = snprintf(sql, MAXSQL, (bIsArtist) ? tsql1 : tsql2, album_info->album_id);
    if ((n < 0) || (n >= MAXSQL))
    {
        LOGE("SQL command length overflow.");
        return;
    }

    sqlite3_stmt *stmt;
    int rc = sqlite3_prepare_v2(c->db, sql, -1, &stmt, nullptr);
    if (rc)
    {
        LOGE("DB: Can't prepare: %s\n", sqlite3_errmsg(c->db));
        LOGD("DB: failing SQL command: %s\n", sql);
        return;
    }

    rc = sqlite3_bind_text(stmt, 1, album_info->album_name, -1, SQLITE_TRANSIENT);
    if ((rc == SQLITE_OK) && (bIsArtist))
    {
        rc = sqlite3_bind_text(stmt, 2, album_info->album_artist, -1, SQLITE_TRANSIENT);
    }

    if (rc == SQLITE_OK)
    {
        /* Execute SQL statement */
        LOGD("DB: execute SQL command: %s\n", sql);
        rc = sqlite3_step(stmt);

        if (rc != SQLITE_DONE)
        {
            LOGE("SQL command failed : \"%s\"", sql);
            const char *zErrMsg = sqlite3_errmsg(c->db);
            LOGE("SQL error: %s (%d)\n", zErrMsg, rc);
        }
        else
        {
            LOGD("Table updated successfully\n");
        }

        rc = sqlite3_finalize(stmt);
        if (rc)
        {
            LOGE("DB: Can't finalize: %s\n", sqlite3_errmsg(c->db));
        }
    }
}


/******************************************************************************
 *
 * collect album information from file table
 *
 * Note that album name and album artist may include quotation characters, so that
 * we cannot simply build an SQL command in text form without doubling the quotes.
 *
 * As a workaround, we do not use sqlite3_exec(), but the more sophisticated
 * combination sqlite3_prepare_v2()/sqlite3_bind_text()/sqlite3_step()/sqlite3_finalize()
 *
 *****************************************************************************/
static void runSqlQueryGetAlbumInfo(Context *c, int64_t album_id, audioAlbumInfo *album_info, int extractFolderIcons, bool bIncrAudioFilesFound)
{
    const char * tsql1 = "SELECT * FROM " FILES_TABLE_NAME " WHERE " FILES_TABLE_TCOL_ALBUM " = ? AND " FILES_TABLE_TCOL_ALBUM_ARTIST " = ?;";
    const char * tsql2 = "SELECT * FROM " FILES_TABLE_NAME " WHERE " FILES_TABLE_TCOL_ALBUM " = ? AND " FILES_TABLE_TCOL_ALBUM_ARTIST " IS NULL;";
    const char * tsql3 = "SELECT * FROM " FILES_TABLE_NAME " WHERE " FILES_TABLE_TCOL_ALBUM_ID " = ?;";
    const char *sql;

    bool bIsId = (album_id != 0);
    bool bIsArtist = (album_info->album_artist[0] != '\0');
    if (bIsId)
    {
        sql = tsql3;
    }
    else
    {
        sql = (bIsArtist) ? tsql1 : tsql2;
    }

    sqlite3_stmt *stmt;
    LOGD("SqLite call: %s\n", sql);
    int rc = sqlite3_prepare_v2(c->db, sql, -1, &stmt, nullptr);
    if (rc)
    {
        LOGE("DB: Can't prepare: %s\n", sqlite3_errmsg(c->db));
        return;
    }

    if (bIsId)
    {
        rc = sqlite3_bind_int64(stmt, 1, album_id);
    }
    else
    {
        rc = sqlite3_bind_text(stmt, 1, album_info->album_name, -1, SQLITE_TRANSIENT);
        if ((rc == SQLITE_OK) && (bIsArtist))
        {
            rc = sqlite3_bind_text(stmt, 2, album_info->album_artist, -1, SQLITE_TRANSIENT);
        }
    }

    (void) rc;
    char path_embed[MAXPATH + 1];   // path of audio file with embedded picture
    int size_embed = 0;

    // album_info has already been initialised to zero
    while((rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        const char *composer  = (const char *) sqlite3_column_text(stmt, FILES_TABLE_COL_COMPOSER);
        const char *performer = (const char *) sqlite3_column_text(stmt, FILES_TABLE_COL_PERFORMER);
        const char *conductor = (const char *) sqlite3_column_text(stmt, FILES_TABLE_COL_CONDUCTOR);
        const char *genre     = (const char *) sqlite3_column_text(stmt, FILES_TABLE_COL_GENRE);
        const char *path      = (const char *) sqlite3_column_text(stmt, FILES_TABLE_COL_PATH);
        int year              =                sqlite3_column_int (stmt, FILES_TABLE_COL_YEAR);
        int duration          =                sqlite3_column_int (stmt, FILES_TABLE_COL_DURATION);
        int track             =                sqlite3_column_int (stmt, FILES_TABLE_COL_TRACK_NO);
        int pic_size          =                sqlite3_column_int (stmt, FILES_TABLE_COL_PIC_SIZE);

        strCpyUnique(album_info->album_composer,  composer);
        strCpyUnique(album_info->album_performer, performer);
        strCpyUnique(album_info->album_conductor, conductor);
        strCpyUnique(album_info->album_genre,     genre);

        if (year > 0)
        {
            if ((album_info->album_first_year == 0) || (year < album_info->album_first_year))
            {
                album_info->album_first_year = year;
            }

            if ((album_info->album_last_year == 0) || (year > album_info->album_last_year))
            {
                album_info->album_last_year = year;
            }
        }

        album_info->album_duration += duration;
        album_info->album_no_tracks++;

        int disk = track % 1000; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        if (disk > album_info->album_no_disks)
        {
            album_info->album_no_disks = disk;
        }

        if (!album_info->album_picture[0])
        {
            // no album picture, yet

            int index = getAlbumPath(path);
            if ((index > 0) && (strncmp(path, album_info->album_path, (size_t) index) != 0))
            {
                // first album path, or it differs from previous one
                if (album_info->album_path[0] == '\0')
                {
                    memcpy(album_info->album_path, path, (size_t) index);
                    album_info->album_path[index] = '\0';
                }
                checkAlbumPicture(album_info, path, ((size_t) index) + 1);
                scaleAlbumPicture(c, album_info);
            }
        }

        if (pic_size > size_embed)
        {
            strcpy(path_embed, path);       // remember audio file with embedded picture
            size_embed = pic_size;
        }

        if (bIncrAudioFilesFound)
        {
            c->nAudioFilesFound++;
        }
    }

    (void) rc;
    LOGD_ALBUM("DB: left loop with rc = %d\n", rc);

    rc = sqlite3_finalize(stmt);
    if (rc)
    {
        LOGE("DB: Can't finalize: %s\n", sqlite3_errmsg(c->db));
    }

    LOGD_ALBUM("DB: found %d matching audio files\n", album_info->album_no_tracks);

    if (!album_info->album_picture[0] && (extractFolderIcons != 0))
    {
        LOGD_ALBUM("DB: no folder picture found so far\n");
        if (size_embed > 0)
        {
            int index = getAlbumPath(path_embed);
            if (index > 0)
            {
                // we can ignore if the extraction fails, because ...
                extractPictureFromAudioFile(c, path_embed, album_info->album_path);
                // ... here we check if the image file now exists
                checkAlbumPicture(album_info, path_embed, ((size_t) index) + 1);
                scaleAlbumPicture(c, album_info);
                if (album_info->album_picture[0])
                {
                    // the image file now exists, thus extraction was obviously successful
                    c->nFolderImagesCreated++;
                }
            }
        }
    }
}


/******************************************************************************
 *
 * build albums database
 *
 *****************************************************************************/
void runSqlCreateAllAlbumsFromFiles(Context *c, int extractFolderIcons)
{
    const char * sql = "SELECT DISTINCT " FILES_TABLE_TCOL_ALBUM ", " FILES_TABLE_TCOL_ALBUM_ARTIST
                       " FROM " FILES_TABLE_NAME " ORDER BY " FILES_TABLE_TCOL_ALBUM ", " FILES_TABLE_TCOL_ALBUM_ARTIST ";";
    sqlite3_stmt *stmt;
    int rc = sqlite3_prepare_v2(c->db, sql, -1, &stmt, nullptr);
    if (rc)
    {
        LOGE("DB: Can't prepare: %s\n", sqlite3_errmsg(c->db));
        return;
    }

//    rc = sqlite3_column_count(stmt);
//    LOGD("DB: Query result has %d columns\n", rc);

    sqlite3_stmt *ins_stmt;
    prepareSqlInsertToAlbumTable(c, &ins_stmt);
    int64_t album_id = c->base_id;

    while((rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        const char *album = (const char *) sqlite3_column_text(stmt, 0);
        const char *album_artist = (const char *) sqlite3_column_text(stmt, 1);
        //const char *path = (const char *) sqlite3_column_text(stmt, 3);
        LOGD_ALBUM("DB: Found album \"%s\" with performer \"%s\"\n", album, album_artist);

        // fill in audio info structure
        audioAlbumInfo info;
        memset(&info, 0, sizeof(info));     // init to zero
        strCpySafe(info.album_name, album);
        strCpySafe(info.album_artist, album_artist);
        runSqlQueryGetAlbumInfo(c, 0, &info, extractFolderIcons, true);
        LOGD("DB: found %d tracks for this album\n", info.album_no_tracks);

        if (info.album_no_tracks > 0)
        {
            info.album_id = album_id;
            executeSqlInsertToAlbumTable(c, &info, ins_stmt);
            executeSqlUpdateAlbumIdsInFilesTable(c, &info);
            album_id++;
        }
    }

    (void) rc;
    LOGD_ALBUM("DB: left loop with rc = %d\n", rc);

    (void) sqlite3_finalize(ins_stmt);

    rc = sqlite3_finalize(stmt);
    if (rc)
    {
        LOGE("DB: Can't finalize: %s\n", sqlite3_errmsg(c->db));
    }
}


/******************************************************************************
 *
 * build one album, given by album id
 *
 *****************************************************************************/
void runSqlCreateAlbumFromFiles(Context *c, int64_t album_id, const char *album_name, const char *album_artist, int extractFolderIcons)
{
    assert(album_id != 0);
    audioAlbumInfo album_info;
    memset(&album_info, 0, sizeof(album_info));

    strCpySafe(album_info.album_name, album_name);
    strCpySafe(album_info.album_artist, album_artist);
    runSqlQueryGetAlbumInfo(c, album_id, &album_info, extractFolderIcons, false);
    LOGD("DB: found %d tracks for this album\n", album_info.album_no_tracks);

    if (album_info.album_no_tracks > 0)
    {
        sqlite3_stmt *ins_stmt;
        prepareSqlInsertToAlbumTable(c, &ins_stmt);
        album_info.album_id = album_id;
        executeSqlInsertToAlbumTable(c, &album_info, ins_stmt);
        (void) sqlite3_finalize(ins_stmt);
    }
}


/******************************************************************************
 *
 * get file id and album information from path
 *
 * In case of success the return values will be found in (*pXXX), otherwise
 * these values will be null or zero.
 *
 * A return value other than zero denotes a severe error.
 *
 *****************************************************************************/
int runSqlQueryFileFromPath
(
    Context *c,
    const char *path,
    int64_t *p_id,
    int64_t *p_album_id,            // may be null
    const char **palbum_name,       // may be null
    const char **palbum_artist      // may be null
)
{
    const char * sql = "SELECT "
            FILES_TABLE_TCOL_ID ","
            FILES_TABLE_TCOL_ALBUM_ID ","
            FILES_TABLE_TCOL_ALBUM ","
            FILES_TABLE_TCOL_ALBUM_ARTIST
            " FROM " FILES_TABLE_NAME " WHERE " FILES_TABLE_TCOL_PATH " = ?;";
    *p_id = 0;
    if (p_album_id != nullptr)
    {
        *p_album_id = 0;
    }
    if (palbum_name != nullptr)
    {
        *palbum_name = nullptr;
    }
    if (palbum_artist != nullptr)
    {
        *palbum_artist = nullptr;
    }

    sqlite3_stmt *stmt;
    LOGD("SqLite call: %s\n", sql);
    int rc = sqlite3_prepare_v2(c->db, sql, -1, &stmt, nullptr);
    if (rc)
    {
        LOGE("DB: Can't prepare: %s\n", sqlite3_errmsg(c->db));
        return eResultSqlError;
    }
    (void) sqlite3_bind_text(stmt, 1, path, -1, SQLITE_TRANSIENT);

    // there should be only one id
    while((rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        *p_id = sqlite3_column_int64(stmt, 0);
        if (p_album_id != nullptr)
        {
            *p_album_id = sqlite3_column_int64(stmt, 1);
            LOGD("DB: read id: %" PRId64 ", album_id: %" PRId64 "\n", *p_id, *p_album_id);
        }
        else
        {
            LOGD("DB: read id: %" PRId64 "\n", *p_id);
        }
        if (palbum_name != nullptr)
        {
            *palbum_name = copyNonEmptyString((const char *) sqlite3_column_text(stmt, 2));
        }
        if (palbum_artist != nullptr)
        {
            *palbum_artist = copyNonEmptyString((const char *) sqlite3_column_text(stmt, 3));
        }
        break;
    }

    (void) rc;
    rc = sqlite3_finalize(stmt);
    if (rc)
    {
        LOGE("DB: Can't finalize: %s\n", sqlite3_errmsg(c->db));
    }

    return 0;
}


/******************************************************************************
 *
 * get album information
 *
 *****************************************************************************/
int runSqlQueryAlbum
(
    Context *c,
    const char *album_name,
    const char *album_artist,
    int64_t *p_album_id
)
{
    const char * tsql1 = "SELECT " ALBUMS_TABLE_TCOL_ID " FROM " ALBUMS_TABLE_NAME " WHERE " ALBUMS_TABLE_TCOL_NAME " = ? AND " ALBUMS_TABLE_TCOL_ARTIST " = ?;";
    const char * tsql2 = "SELECT " ALBUMS_TABLE_TCOL_ID " FROM " ALBUMS_TABLE_NAME " WHERE " ALBUMS_TABLE_TCOL_NAME " = ? AND " ALBUMS_TABLE_TCOL_ARTIST " IS NULL;";
    const char *sql;

    *p_album_id = 0;
    bool bIsArtist = !strZeroOrEmpty(album_artist);
    sql = (bIsArtist) ? tsql1 : tsql2;

    sqlite3_stmt *stmt;
    LOGD("SqLite call: %s\n", sql);
    int rc = sqlite3_prepare_v2(c->db, sql, -1, &stmt, nullptr);
    if (rc)
    {
        LOGE("DB: Can't prepare: %s\n", sqlite3_errmsg(c->db));
        return eResultSqlError;
    }

    rc = sqlite3_bind_text(stmt, 1, album_name, -1, SQLITE_TRANSIENT);
    if ((rc == SQLITE_OK) && (bIsArtist))
    {
        (void) sqlite3_bind_text(stmt, 2, album_artist, -1, SQLITE_TRANSIENT);
    }

    // we can stop after the first "hit"
    while((rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        *p_album_id = sqlite3_column_int64(stmt, 0);
        LOGI("DB: queried album_id: %" PRId64 "\n", *p_album_id);
        break;
    }

    (void) rc;
    rc = sqlite3_finalize(stmt);
    if (rc)
    {
        LOGE("DB: Can't finalize: %s\n", sqlite3_errmsg(c->db));
    }

    return 0;
}




/******************************************************************************
 *
 * get changed files
 *
 *****************************************************************************/
pathList *runSqlQueryChangedFiles
(
    Context *c,
    int *p_all,
    int *p_chg
)
{
    const char * sql = "SELECT "
                       FILES_TABLE_TCOL_PATH ","
                       FILES_TABLE_TCOL_MTIME
                       " FROM " FILES_TABLE_NAME " ORDER BY " FILES_TABLE_TCOL_PATH ";";

    *p_all = *p_chg = 0;

    sqlite3_stmt *stmt;
    LOGD("SqLite call: %s\n", sql);
    int rc = sqlite3_prepare_v2(c->db, sql, -1, &stmt, nullptr);
    if (rc)
    {
        LOGE("DB: Can't prepare: %s\n", sqlite3_errmsg(c->db));
        return nullptr;
    }

//    rc = sqlite3_bind_text(stmt, 1, path, -1, SQLITE_TRANSIENT);

    //
    // gather a linked list of paths to be updated
    //

    struct pathList *first = nullptr;
    struct pathList *last = nullptr;
    while((rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        (*p_all)++;
        int idx = 0;
        const char *path = copyNonEmptyString((const char *) sqlite3_column_text(stmt, idx++));
        auto mtime = (time_t) sqlite3_column_int64(stmt, idx);

        LOGD("Check file \"%s\"\n", path);
        struct stat statbuf; // NOLINT(cppcoreguidelines-pro-type-member-init,hicpp-member-init)
        int res = stat(path, &statbuf);
        if ((res != 0) || (mtime != statbuf.st_mtim.tv_sec))
        {
            (*p_chg)++;
            LOGI("file deleted or changed: \"%s\"\n", path);
            auto entry = (pathList *) malloc(sizeof(*first));
            entry->path = path;
            entry->bExists = (res == 0);
            entry->pNext = nullptr;

            if (first == nullptr)
            {
                first = last = entry;
            }
            else
            {
                last->pNext = entry;
                last = entry;
            }
        }
        else
        {
            free((void *) path);
        }

        c->nAudioFilesFound++;
        doProgressCallback(c, eResultProgress);
    }

    (void) rc;
    rc = sqlite3_finalize(stmt);
    if (rc)
    {
        LOGE("DB: Can't finalize: %s\n", sqlite3_errmsg(c->db));
    }

    return first;
}


