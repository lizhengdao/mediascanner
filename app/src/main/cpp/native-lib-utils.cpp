/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma ide diagnostic ignored "EmptyDeclOrStmt"

#include <string.h> // NOLINT(modernize-deprecated-headers,hicpp-deprecated-headers)
#include <inttypes.h> // NOLINT(modernize-deprecated-headers,hicpp-deprecated-headers)
#include <stdlib.h> // NOLINT(modernize-deprecated-headers,hicpp-deprecated-headers)
#include <assert.h> // NOLINT(modernize-deprecated-headers,hicpp-deprecated-headers)

#include "native-lib-utils.h"


static const char * sVarious = "≠";


/************************************************************************************
 *
 * safe string copy with automatic truncation
 *
 ***********************************************************************************/
void strCpySafe(char *dst, const char *src)
{
    if (src == nullptr)
    {
        dst[0] = '\0';
    }
    else
    {
        strncpy(dst, src, MAXSTR - 1);
        dst[MAXSTR - 1] = '\0';
    }
}


#if 0
/************************************************************************************
 *
 * safe string compare
 *
 ***********************************************************************************/
extern int strCmpSafe(const char *s1, const char *s2)
{
    if (strZeroOrEmpty(s1))
    {
        if (strZeroOrEmpty(s2))
            return 0;
        else
            return -1;
    }
    else
    if (strZeroOrEmpty(s2))
        return 1;
    else
        return strcmp(s1, s2);
}
#endif


/************************************************************************************
 *
 * copy unique string
 *
 ***********************************************************************************/
void strCpyUnique(char *dst, const char *src)
{
    if (dst[0] != sVarious[0])
    {
        if (dst[0] == '\0')
        {
            strCpySafe(dst, src);   // not set before. Now dst may also be empty.
        }
        else
        if ((src == nullptr) || (strcmp(src, dst) != 0))
        {
            //LOGW("strCpyUnique(dst: %s, src: %s\n", dst, src);
            strcpy(dst, sVarious);  // values differ
        }
    }
}


/************************************************************************************
 *
 * make a copy of the strings for later use
 *
 ***********************************************************************************/
const char *copyNonEmptyString(const char *s)
{
    if (s == nullptr)
    {
        return nullptr;
    }
    const char *t = s;
    SKIP_SPACE(t);
    if (*t == '\0')
    {
        return nullptr;
    }

    size_t l = strlen(t) + 1;   // including '\0'
    char *sn = (char *) malloc(l);
    memcpy(sn, t, l);
    return sn;
}


/************************************************************************************
 *
 * helper to get int from long
 *
 ***********************************************************************************/
int fromLong(long l)
{
    if ((l < 0) || (l > INT32_MAX))
    {
        return 0;
    }
    else
    {
        return (int) l;
    }
}


/************************************************************************************
 *
 * helper to evaluate strings like "n/m" or "n"
 *
 * return 0 if no error
 ***********************************************************************************/
int evalNfromM(int *n, int *m, const char *s)
{
    if (s == nullptr)
    {
        // no string
        *n = *m = 0;
        return 0;
    }
    char *endptr;
    long l;
    l = strtol(s, &endptr, 10);
    if (endptr == s)
    {
        // malformed string
        *n = *m = 0;        // both N and M invalid
        return -1;
    }
    *n = fromLong(l);
    SKIP_SPACE(endptr)
    if (*endptr == '\0')
    {
        *m = 0;     // no M
        return 0;
    }
    if (*endptr != '/')
    {
        *m = 0;     // M invalid
        return -2;
    }
    endptr++;
    SKIP_SPACE(endptr)
    l = strtol(s, &endptr, 10);
    if (endptr == s)
    {
        // malformed M
        *m = 0;        // M invalid
        return -3;
    }
    *m = fromLong(l);
    return 0;
}


/******************************************************************************
 *
 * helper to get a file name from a path
 *
 *****************************************************************************/
const char * getFileNameFromPath(const char *path)
{
    const char *s = strrchr(path, '/');
    if (s == nullptr)
    {
        return path;
    }

    return s + 1;
}


/******************************************************************************
 *
 * helper to get a directory name
 *
 * /a/bb/ccc -> ccc
 * /a/bb/ccc/ -> ccc
 * /a/bb/ccc/// -> ccc
 * / -> /
 *
 *****************************************************************************/
void getDirNameFromPath(const char *path, long l, int *start_index, int *stop_index)
{
    if (l <= 0)
    {
        *start_index = *stop_index = 0;
        return;
    }

    // skip trailing '/'
    const char *se = path + l - 1;
    while ((se > path) && (*se == '/'))
    {
        se--;
    }
    const char *ss = se;
    do
    {
        ss--;
    }
    while((ss > path) && (*ss != '/'));

    if (*ss == '/')
    {
        ss++;
    }
    *stop_index = (int) (se - path + 1);
    *start_index = (int) (ss - path);
}


/******************************************************************************
 *
 * helper to get a directory name
 *
 *****************************************************************************/
void extractDirNameFromPath
(
    const char *path,       // src
    long lp,                // length of path
    char *fname,            // dst
    const char **startp     // out: pointer to fname inside path
)
{
    int n, m;
    getDirNameFromPath(path, lp, &n, &m);
    assert(m >= n);
    auto l = (size_t) (m - n);
    if (l > MAXFNAME)
    {
        l = MAXFNAME;
    }
    strncpy(fname, path + n, (size_t) (m - n));
    fname[l] = '\0';
    *startp = path + n;
}
