/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Java/JNI
#include <jni.h>

#include <sqlite3.h>

#define LOG_TAG    "native-lib"
#define FILES_TABLE_NAME "MUSICFILES"
#define ALBUMS_TABLE_NAME "ALBUMS"
#define INFO_TABLE_NAME "INFORMATION"
#define DB_VERSION 2

#define xstr(s) str(s)
#define str(s) #s

#define MAXSQL 1024         // SQL commands
#define NUMELEMS(a) (sizeof(a)/sizeof((a)[0]))

#define  LOGD(...)  //__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#define  LOGD_ALBUM(...)  //__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGI_ALBUM(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGD_ID3V1(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)

#define MAXFILES 99999 // 100        // for debugging

// file table column indices
#define FILES_TABLE_COL_ID              0
#define FILES_TABLE_COL_TRACK_NO        1
#define FILES_TABLE_COL_TITLE           2
#define FILES_TABLE_COL_ALBUM           3
#define FILES_TABLE_COL_ALBUM_ID        4
#define FILES_TABLE_COL_DURATION        5
#define FILES_TABLE_COL_GROUPING        6
#define FILES_TABLE_COL_SUBTITLE        7
#define FILES_TABLE_COL_COMPOSER        8
#define FILES_TABLE_COL_PERFORMER       9
#define FILES_TABLE_COL_ALBUM_ARTIST    10
#define FILES_TABLE_COL_CONDUCTOR       11
#define FILES_TABLE_COL_GENRE           12
#define FILES_TABLE_COL_YEAR            13
#define FILES_TABLE_COL_PATH            14
#define FILES_TABLE_COL_PIC_TYPE        15      // 0: none, 1: jpeg, 2: png
#define FILES_TABLE_COL_PIC_SIZE        16
#define FILES_TABLE_COL_TAG_TYPE        17      // tagType
#define FILES_TABLE_COL_MTIME           18      // modification time [s]

#define FILES_TABLE_TCOL_ID              "_id"
#define FILES_TABLE_TCOL_TRACK_NO        "track"
#define FILES_TABLE_TCOL_TITLE           "title"
#define FILES_TABLE_TCOL_ALBUM           "album"
#define FILES_TABLE_TCOL_ALBUM_ID        "album_id"
#define FILES_TABLE_TCOL_DURATION        "duration"
#define FILES_TABLE_TCOL_GROUPING        "grouping"
#define FILES_TABLE_TCOL_SUBTITLE        "subtitle"
#define FILES_TABLE_TCOL_COMPOSER        "composer"
#define FILES_TABLE_TCOL_PERFORMER       "artist"
#define FILES_TABLE_TCOL_ALBUM_ARTIST    "album_artist"
#define FILES_TABLE_TCOL_CONDUCTOR       "conductor"
#define FILES_TABLE_TCOL_GENRE           "genre"
#define FILES_TABLE_TCOL_YEAR            "year"
#define FILES_TABLE_TCOL_PATH            "path"
#define FILES_TABLE_TCOL_PIC_TYPE        "pic_type"      // 0: none, 1: jpeg, 2: png
#define FILES_TABLE_TCOL_PIC_SIZE        "pic_size"
#define FILES_TABLE_TCOL_TAG_TYPE        "tag_type"      // tagType
#define FILES_TABLE_TCOL_MTIME           "mtime"        // modification time [s]


#define ALBUMS_TABLE_COL_ID             0
#define ALBUMS_TABLE_COL_NAME           1
#define ALBUMS_TABLE_COL_ARTIST         2
#define ALBUMS_TABLE_COL_COMPOSER       3
#define ALBUMS_TABLE_COL_PERFORMER      4
#define ALBUMS_TABLE_COL_CONDUCTOR      5
#define ALBUMS_TABLE_COL_GENRE          6
#define ALBUMS_TABLE_COL_FIRST_YEAR     7
#define ALBUMS_TABLE_COL_LAST_YEAR      8
#define ALBUMS_TABLE_COL_NO_TRACKS      9
#define ALBUMS_TABLE_COL_DURATION       10
#define ALBUMS_TABLE_COL_PATH           11
#define ALBUMS_TABLE_COL_PICTURE        12

#define ALBUMS_TABLE_TCOL_ID            "_id"
#define ALBUMS_TABLE_TCOL_NAME          "album"
#define ALBUMS_TABLE_TCOL_ARTIST        "artist"            // from tags
#define ALBUMS_TABLE_TCOL_COMPOSER      "composer"          // common composer, if any
#define ALBUMS_TABLE_TCOL_PERFORMER     "performer"         // common performer, if any
#define ALBUMS_TABLE_TCOL_CONDUCTOR     "conductor"
#define ALBUMS_TABLE_TCOL_GENRE         "genre"
#define ALBUMS_TABLE_TCOL_FIRST_YEAR    "minyear"
#define ALBUMS_TABLE_TCOL_LAST_YEAR     "maxyear"
#define ALBUMS_TABLE_TCOL_NO_TRACKS     "numsongs"
#define ALBUMS_TABLE_TCOL_DURATION      "duration"
#define ALBUMS_TABLE_TCOL_PATH          "path"
#define ALBUMS_TABLE_TCOL_PICTURE       "album_art"

enum tagType
{
    eTagUnknown = 0,
    eTagId3v1 = 1,
    eTagId3v2 = 2,
    eTagMp4 = 10,
    eTagOgg = 11
};


enum tagNumbers
{
    idAlbum = 0,                // mp3: TALB    mp4: ©alb       flac: ALBUM
    idArtist = 1,               // mp3: TPE1    mp4: ©ART       flac: ARTIST
    idAlbumArtist = 2,          // mp3: TPE2    mp4: aART       flac: ALBUMARTIST
    idConductor = 3,            // mp3: TPE3    mp4: com.apple.iTunes.CONDUCTOR     (should be ©con?)
    idGrouping = 4,             // mp3: TIT1    mp4: ©grp       flac: GROUPING
    idTitle = 5,                // mp3: TIT2    mp4: ©nam       flac: TITLE
    idSubtitle = 6,             // mp3: TIT3    mp4: com.apple.iTunes.SUBTITLE      (should be desc)
    idComposer = 7,             // mp3: TCOM    mp4: ©wrt       flac: COMPOSER
    idDiscNo = 8,               // mp3: TPOS    mp4: disk       flac: DISCNUMBER
//  idDiscTotal = 9,            // mp3: TPOS    mp4: disk
    idTrack = 10,               // mp3: TRCK    mp4: trkn       flac: TRACKNUMBER
//  idTrackTotal = 11,          // mp3: TRCK    mp4: trkn
    idGenre = 12,               // mp3: TCON    mp4: ©gen       flac: GENRE
    idYear = 13,                // mp3: TYER    mp4: ©day       flac: DATE
    idComment = 14,             // mp3: COMM    mp4: ©cmt       flac: COMMENT
// iTunes:
    idAppleMovement = 15,       // mp3: MVNM   mp4: ©mvn
    idAppleMovementNo = 16,     // mp3: MVIN   mp4: ©mvi
    idAppleMovementTotal = 17,  // mp3: MVIN   mp4: ©mvc
    idAppleMp3Work = 18,        // mp3: GRP1   mp4: <-->
    idAppleMp4Work = 19,        // mp3: <-->   mp4: ©wrk
// derived fields
    idAppleWork = 20,           // mp3: GRP1   mp4: ©wrk
//  idCombinedMovement = 21,    // combine movement or title information
// number of fields
    idNum = 22
};


typedef struct
{
    // to audio file database
    int64_t id;
    int track_disc_no;          // 1000 * discno + trackno
    const char *title;
    const char *album;
    int64_t album_id;
    int duration;
    const char *grouping;
    const char *subtitle;
    const char *composer;
    const char *performer;
    const char *album_artist;
    const char *conductor;
    const char *genre;
    int year;
    const char *path;

    // intermediates
    int track_no;
    int disc_no;

    // for album database
    int no_tracks;
    int no_discs;

    // raw text tags
    const char *tags[idNum];    // raw

    // picture (album art)
    int pic_type;
    int pic_size;

    // file information
    time_t mtime;               // file modification time

    // debug
    tagType tag_type;
} audioFileInfo;

typedef struct
{
    int64_t album_id;
    char album_name[MAXSTR];
    char album_artist[MAXSTR];
    char album_composer[MAXSTR];
    char album_performer[MAXSTR];
    char album_conductor[MAXSTR];
    char album_genre[MAXSTR];
    int album_first_year;
    int album_last_year;
    int album_no_tracks;
    int album_no_disks;
    int album_duration;
    char album_path[MAXPATH + 1];
    char album_picture[MAXPATH + 1];
} audioAlbumInfo;

typedef struct
{
    // SQL stuff
    sqlite3 *db;
    sqlite3_stmt *ins_stmt;         // prepared INSERT statement
    int64_t sql_last_qres0;             // last query result [0]

    int64_t base_id;
    int curr_id;
    int mode;

    audioFileInfo info;
    char fname[MAXFNAME + 1];        // used in case of undefined title

    // result:
    int nAudioFilesFound;
//    int nAudioFilesMp3v1;
//    int nAudioFilesMp3v2;
//    int nAudioFilesMp4;
    int nDirectoriesFound;
    int nAlbumsFound;
    int nFolderImagesCreated;
    int nFolderImagesCreationFailed;
    int nExistingFolderImagesScaled;
    int nAlbumPicturesAdded;
    int nAlbumPicturesRemoved;

    // callback to Java
    JNIEnv* jenv;
    jobject jobj;
    jmethodID jmethodProgress;
    jmethodID jmethodScalePicture;
} Context;


typedef enum
{
    eMp3 = 0,
    eMp4 = 1,
    eOgg = 2,
    eFlac = 3,
    eInvalid = -1
} AudioFileType;


// final result passed to callback
typedef enum
{
    eResultMalformedPath = -4,
    eResultSqlError = -3,
    eResultCannotOpenDb = -2,
    eResultInvalidCmd = -1,
    eResultProgress = 0,
    eResultFileScan = 1,
    eResultFileProcess = 2,
    eResultDbCreatedOrOpened = 3,
    eResultDone = 4,
    eResultAlbumScan = 5,
    eResultTablesRemoved = 6,
    eResultAlbumPicturesUpdated = 7
} resultType;

extern int getAlbumPath(const char *audioFilepath);
extern void checkAlbumPicture(audioAlbumInfo *album_info, const char *path, size_t index);
extern void scaleAlbumPicture(Context *c, audioAlbumInfo *album_info);
extern void extractPictureFromAudioFile(Context *c, const char *audio_file_path, const char *destination_path);
extern void doProgressCallback(Context *c, int is_final);

extern char album_picture_base_fname[];
