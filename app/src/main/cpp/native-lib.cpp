/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma ide diagnostic ignored "EmptyDeclOrStmt"
#pragma ide diagnostic ignored "cppcoreguidelines-macro-usage"
#pragma ide diagnostic ignored "cppcoreguidelines-avoid-goto"

//#include <string>
#include <dirent.h>
#include <cerrno>
#include <sys/time.h> // NOLINT(modernize-deprecated-headers,hicpp-deprecated-headers)
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>

// Android
#include <android/log.h>

// simple taglib interface
//#include "tag_c.h"

// advanced taglib interface
#include <mpeg/id3v2/id3v2tag.h>
#include <mpeg/id3v1/id3v1tag.h>
#include <mpeg/id3v2/frames/attachedpictureframe.h>
#include "mpegfile.h"
#include <mp4/mp4file.h>
#include <ogg/flac/oggflacfile.h>
#include <ogg/vorbis/vorbisfile.h>
#include <flac/flacfile.h>

#include "native-lib-utils.h"
#include "native-lib.h"
#include "native-lib-db.h"
#include "native-lib-tags.h"
#include "native-cmd.h"


char album_picture_base_fname[64] = "albumart";
static char lastResult[256]; // NOLINT(cppcoreguidelines-avoid-magic-numbers)



#define JNIFN(TYPE, FN) extern "C" JNIEXPORT TYPE JNICALL Java_de_kromke_andreas_mediascanner_ScannerManager_ ## FN

/*
JNIFN(jstring, stringFromJNI)
(
    JNIEnv* env,
    jobject)
{
    (void) env;
    //std::string hello = "Hello from C++";
    //return env->NewStringUTF(hello.c_str());
    return env->NewStringUTF(lastResult);
}
*/



/************************************************************************************
 *
 * evaluate file info from raw tags
 *
 * note that these attribute are preset:
 *
 *  title as filename without extension
 *  album as directory name
 *  duration
 *  path
 *
 ***********************************************************************************/
static void initFileInfoFromTagsAndStat
(
    audioFileInfo *info
)
{
    int m;
    int res;
    const char *s;

    //
    // get file modification time
    //

    struct stat statbuf; // NOLINT(cppcoreguidelines-pro-type-member-init,hicpp-member-init)
    res = stat(info->path, &statbuf);
    if (res == 0)
    {
        info->mtime = statbuf.st_mtim.tv_sec;
    }
    else
    {
        LOGE("   cannot stat:  \"%s\"\n", info->path);
        info->mtime = 0;
    }

    //
    // combine track and disc number to save some sort order work
    // for mp4: numerical values have already been set
    //

    if ((info->track_no == 0) && (info->no_tracks == 0))
    {
        s = info->tags[idTrack];
        res = evalNfromM(&info->track_no, &info->no_tracks, s);
        if (res != 0)
        {
            LOGW("   malformed n/m string for track:  \"%s\"\n", s);
        }
    }

    if ((info->disc_no == 0) && (info->no_discs == 0))
    {
        s = info->tags[idDiscNo];
        res = evalNfromM(&info->disc_no, &info->no_discs, s);
        if (res != 0)
        {
            LOGW("   malformed n/m string for disc:  \"%s\"\n", s);
        }
    }

    info->track_disc_no = 1000 * info->disc_no + info->track_no; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

    //
    // year is numerical, i.e. we ignore m
    //

    s = info->tags[idYear];
    res = evalNfromM(&info->year, &m, s);
    if ((res != 0) || (m != 0))
    {
        LOGW("   malformed n string for year:  \"%s\"\n", s);
    }

    //
    // In case of missing album use the directory name
    //

    s = info->tags[idAlbum];
    if (s != nullptr)
    {
        info->album = s;
    }

    //
    // just copy:
    //  performer ("artist")
    //  album performer ("album artist")
    //  subtitle
    //  composer
    //  conductor
    //

    info->performer = info->tags[idArtist];
    info->album_artist = info->tags[idAlbumArtist];
    info->subtitle = info->tags[idSubtitle];
    info->composer = info->tags[idComposer];
    info->conductor = info->tags[idConductor];

    //
    // prefer Apple Movement tag to standard tag
    //

    s = info->tags[idAppleMovement];
    if (s != nullptr)
    {
        info->title = s;
    }
    else
    {
        s = info->tags[idTitle];
        if (s != nullptr)
        {
            info->title = s;
        }
    }

    //
    // prefer Apple Work tag to standard tag
    //

    s = info->tags[idAppleWork];
    if (s != nullptr)
    {
        info->grouping = s;
    }
    else
    {
        info->grouping = info->tags[idGrouping];
    }

    //
    // repair strange genres (iTunes bug?)
    //

    s = info->tags[idGenre];
    if (s != nullptr)
    {
        if (!strcmp(s, "(32)"))
        {
            info->genre = "Classical";
        }
        else
        {
            info->genre = s;
        }
    }
}


/************************************************************************************
 *
 * deallocate dynamic memory
 *
 ***********************************************************************************/
static void freeTags(audioFileInfo *info)
{
    for (int i = 0; i < idNum; i++)
    {
        if (i == idAppleWork)
        {
            // skip derived tags
            continue;
        }

        if (info->tags[i] != nullptr)
        {
            free((void *) info->tags[i]);
            info->tags[i] = nullptr;
        }
    }
}


/************************************************************************************
 *
 * decide if file is an audio file
 *
 ***********************************************************************************/
static AudioFileType isAudioFile(const char *fname, const char **pos)
{
    if (fname[0] == '.')
    {
        return eInvalid;    // skip Apple files ._bla or similar
    }

    static const char *exts[] =
    {
        ".mp3",
        ".mp4",
        ".m4a",
        ".flac",
        ".ogg"
    };

    *pos = strrchr(fname, '.');
    if (*pos != nullptr)
    {
        int i;
        for (i = 0; i < NUMELEMS(exts); i++)
        {
            if (!strcasecmp(*pos, exts[i]))
            {
                break;
            }
        }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "missing_default_case"
        switch(i) // NOLINT(hicpp-multiway-paths-covered)
        {
            case 0: return eMp3;
            case 1:
            case 2: return eMp4;
            case 3: return eFlac;
            case 4: return eOgg;
        }
#pragma clang diagnostic pop
    }

    return eInvalid;
}


/************************************************************************************
 *
 * open audio file and extract folder.png or folder.jpg
 *
 ***********************************************************************************/
void extractPictureFromAudioFile(Context *c, const char *audio_file_path, const char *destination_path)
{
    LOGI("looking for pictures in file \"%s\"\n", c->info.path);
    const char *dotpos;
    AudioFileType audioType = isAudioFile(audio_file_path, &dotpos);
    switch(audioType)
    {
        case eMp3:  extractPictureMp3      (c, audio_file_path, destination_path); break;
        case eMp4:  extractPictureMp4      (c, audio_file_path, destination_path); break;
        case eFlac: extractPictureFlac     (c, audio_file_path, destination_path); break;
        case eOgg:  extractPictureOggVorbis(c, audio_file_path, destination_path); break;
        default:
            break;
    }
}


/******************************************************************************
 *
 * helper to check if directory name is CD<n> or just <n>
 *
 * returns <n> or -1 in case of no match
 *
 *****************************************************************************/
static int checkDirNameIsCdNumber(const char *dirname, int l)
{
    static const char *prefix = "CD";
    static const int prefixlen = 2;

    if ((l > prefixlen) && !strncasecmp(dirname, prefix, strlen(prefix)))
    {
        dirname += prefixlen;
        l -= prefixlen;
        char *endptr;
        long n = strtol(dirname, &endptr, 10); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        if ((n > 0) && (endptr > dirname) && (endptr - dirname == l))
        {
            return (int) n;     // ignore overflow here
        }
    }

    return -1;
}


/************************************************************************************
 *
 * get album path from file name
 *
 * returns index of first character after path (which will then be without trailing "/")
 *
 ***********************************************************************************/
int getAlbumPath(const char *audioFilepath)
{
    int index = 0;
    LOGD_ALBUM("getAlbumPath() : audio file is \"%s\"\n", audioFilepath);
    const char *endptr = strrchr(audioFilepath, '/');
    if (endptr != nullptr)
    {
        index = (int) (endptr - audioFilepath);
        LOGD_ALBUM("getAlbumPath() : directory is \"%.*s\"\n", index, audioFilepath)
        const char *startptr = endptr;      // last character of containing directory
        while ((startptr > audioFilepath) && (startptr[-1] != '/'))
        {
            startptr--;
        }

        int l = (int) (endptr - startptr);
        LOGD_ALBUM("getAlbumPath() : super directory name is \"%.*s\"\n", l, startptr);
        int cdnum = checkDirNameIsCdNumber(startptr, l);
        if (cdnum > 0)
        {
            index = (int) (startptr - audioFilepath - 1);
            LOGD_ALBUM("getAlbumPath() : super directory is \"%.*s\"\n", index, audioFilepath);
        }
    }

    return index;
}


/************************************************************************************
 *
 * look for folder.jpg and folder.png and legacy: cover.jpg
 *
 ***********************************************************************************/
void checkAlbumPicture(audioAlbumInfo *album_info, const char *path, size_t index)
{
    char fname[MAXPATH + 1];
    struct stat statbuf; // NOLINT(cppcoreguidelines-pro-type-member-init,hicpp-member-init)
    static const char * const fnames[] =
    {
        "AlbumArt.jpg",
        "albumart.jpg",
        "folder.jpg",
        "folder.png",
        "cover.jpg",
        nullptr
    };

    strncpy(fname, path, index);
    const char * const *pp = fnames;
    const char *p;
    while((p = *pp++) != nullptr)
    {
        strcpy(fname + index, p);
        LOGD("checkAlbumPicture(check for \"%s\"\n", fname);
        int res = stat(fname, &statbuf);

        // directory entry must exist and must be a regular file
        if ((res == 0) && ((statbuf.st_mode & S_IFMT) == S_IFREG)) // NOLINT(hicpp-signed-bitwise)
        {
            strcpy(album_info->album_picture, fname);
            LOGD("checkAlbumPicture() : success");
            return;
        }
    }
    LOGD("checkAlbumPicture() : failed");
}


/************************************************************************************
 *
 * scale album picture
 *
 ***********************************************************************************/
void scaleAlbumPicture(Context *c, audioAlbumInfo *album_info)
{
    if (album_info->album_picture[0])
    {
        // call Java from C++
        jstring jpath = c->jenv->NewStringUTF(album_info->album_picture);
        int rc = c->jenv->CallIntMethod(c->jobj, c->jmethodScalePicture, jpath,
                                    1 /* existing picture */);
        if (rc > 0)
        {
            c->nExistingFolderImagesScaled++;
        }
        c->jenv->DeleteLocalRef(jpath);
    }
}


/************************************************************************************
 *
 * handle a single audio file
 *
 * bExists: file exists or not
 *
 ***********************************************************************************/
static void handleSingleAudioFile(char *path, bool bExists, Context *c)
{
    char dirname[MAXFNAME + 1];      // used in case of undefined album

    const char *fname = getFileNameFromPath(path);

    // get directory name as fallback solution for missing album information
    long lp = fname - path;
    const char *stp;
    extractDirNameFromPath(path, lp, dirname, &stp);
    //LOGI("opendir() directory name is \"%s\"\n", dirname);

    int cdnum = checkDirNameIsCdNumber(dirname, (int) strlen(dirname));
    if (cdnum > 0)
    {
        char super_dirname[MAXFNAME + 1];
        const char *sstp;
        extractDirNameFromPath(path, stp - path, super_dirname, &sstp);
        //LOGI("opendir() super directory name is \"%s\"\n", super_dirname);
        //LOGI("opendir() super directory is \"%.*s\"\n", (int) (stp - path), path);
    }

    // file
    const char *dotpos;
    AudioFileType audioType = isAudioFile(fname, &dotpos);
    if (audioType != eInvalid)
    {
        size_t fl = dotpos - fname;     // length of file name without extension
        if (fl > MAXFNAME)
        {
            fl = MAXFNAME;       // truncate file name
        }
        memcpy(c->fname, fname, fl);
        c->fname[fl] = '\0';

        //handleAudioFile(path);      // handle
        memset(&c->info, 0, sizeof(c->info));
        c->info.id = c->base_id + (c->curr_id++);
        c->info.path = path;
        c->info.title = c->fname;           // default value
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreturn-stack-address"
        c->info.album = dirname;        // default value
#pragma clang diagnostic pop

        const char *new_album_name;
        const char *new_album_artist;
        int64_t new_album_id;

        if (bExists)
        {
            switch (audioType)
            {
                case eMp3:
                    handleMp3(c);
                    break;
                case eMp4:
                    handleMp4(c);
                    break;
                case eFlac:
                    handleFlac(c);
                    break;
                case eOgg:
                    handleOggVorbis(c);
                    break;
                default:
                    handleMp3(c);        // make lint happy...
                    break;
            }

            initFileInfoFromTagsAndStat(&c->info);
            LOGI("DB: new album name is \"%s\"\n", c->info.album);
            LOGI("DB: new album artist is \"%s\"\n", c->info.album_artist);
            new_album_name = c->info.album;
            new_album_artist = c->info.album_artist;
        }
        else
        {
            new_album_name = nullptr;
            new_album_artist = nullptr;
        }

        const char *old_album_name = nullptr;
        const char *old_album_artist = nullptr;
        int64_t old_album_id;
        bool bCreateNewAlbum;

        //
        // get old album id from file table entry
        //

        int rc = runSqlQueryFileFromPath(c, path, &c->info.id, &old_album_id, &old_album_name, &old_album_artist);
        if (rc != 0)
        {
            // severe db error, should not happen
            LOGE("DB: Can't find audio file in db: \"%s\"s\n", path);
            old_album_id = 0;
            c->info.id = 0;
        }
        else
        {
            LOGI("DB: old file id is %" PRId64 "\n", c->info.id);
            LOGI("DB: old album name is \"%s\"\n", old_album_name);
            LOGI("DB: old album artist is \"%s\"\n", old_album_artist);
            LOGI("DB: old album id is %"
                         PRId64
                         "\n", old_album_id);
        }

        //
        // get new album id from file metadata
        //

        if (bExists)
        {
            (void) runSqlQueryAlbum(c, new_album_name, new_album_artist, &new_album_id);
            bCreateNewAlbum = (new_album_id == 0);
            if (bCreateNewAlbum)
            {
                // there is no album yet with this album name and artist
                new_album_id = getMaxAlbumId(c);    // make sure to get free id
                new_album_id = (new_album_id <= 0) ? c->base_id : (new_album_id + 1);
                LOGI("DB: new album id will be %"
                             PRId64
                             "\n", new_album_id);
            }
            else
            {
                // the album already exists, we will just expand it
                LOGI("DB: new album id is %"
                             PRId64
                             "\n", new_album_id);
            }

            if (c->info.id != 0)
            {
                //
                // file exists and is in database, so this is a changed file. Remove and recreate
                //
                rc = removeIdFromSqlTable(c, FILES_TABLE_NAME, c->info.id);
                if (rc != 0)
                {
                    LOGE("DB: Can't remove audio file %"
                                 PRId64
                                 " from db\n", c->info.id);
                }
            }
            else
            {
                //
                // file exists, but not in database, so this is a new file
                //

                c->info.id = getMaxFileId(c);
                c->info.id = (c->info.id <= 0) ? c->base_id : (c->info.id + 1);
                LOGI("new file id will be %"
                             PRId64
                             "\n", c->info.id);
            }

            c->info.album_id = new_album_id;
            assert(c->info.album_id != 0);
            rc = executeSqlInsertToFileTable(c, &c->info);       // create resp. recreate
            if (rc == 0)
            {
                LOGI("audio file id %"
                         PRId64
                         " inserted successfully\n", c->info.id);
            }
        }
        else
        {
            if (c->info.id != 0)
            {
                //
                // file does not exist, but is in database, so this is a deleted file
                //

                rc = removeIdFromSqlTable(c, FILES_TABLE_NAME, c->info.id);
                if (rc != 0)
                {
                    LOGE("DB: Can't remove audio file %"
                                 PRId64
                                 " from db\n", c->info.id);
                }
            }

            new_album_id = 0;
            bCreateNewAlbum = false;
        }

        bool bNoAlbumChange = ((old_album_id != 0) && (old_album_id == new_album_id));

        if (!bNoAlbumChange && (rc == 0))
        {
            //
            // rebuild old album, if valid
            //

            if (old_album_id != 0)
            {
                (void) removeIdFromSqlTable(c, ALBUMS_TABLE_NAME, old_album_id);
                runSqlCreateAlbumFromFiles(c, old_album_id, old_album_name, old_album_artist, 0);
            }

            //
            // (re)build new album, if valid and different from old one
            //

            if ((new_album_id != 0) && (new_album_id != old_album_id))
            {
                if (!bCreateNewAlbum)
                {
                    // we only have to process this if the new album already exists
                    (void) removeIdFromSqlTable(c, ALBUMS_TABLE_NAME, new_album_id);
                }
                runSqlCreateAlbumFromFiles(c, new_album_id, new_album_name, new_album_artist, 0);
            }
        }

        if (old_album_name != nullptr)
        {
            free((void *) old_album_name);
        }
        if (old_album_artist != nullptr)
        {
            free((void *) old_album_artist);
        }
        freeTags(&c->info);                // deallocate strings
        c->info.album = nullptr;    // bug in Android Studio, this should prevent "value escapes local scope"
    }
}


/************************************************************************************
 *
 * called from scanPathRecursively to handle a single audio file
 *
 ***********************************************************************************/
static void scanHandleFile
(
    char *path,
    Context *c,
    const char *dirname,
    size_t len,     // path length
    int *nFilesInDir,
    const char *d_name
)
{
    const char *dotpos;
    AudioFileType audioType = isAudioFile(d_name, &dotpos);
    if (audioType != eInvalid)
    {
        size_t fl = dotpos - d_name;     // length of file name without extension
        if (fl > MAXFNAME)
        {
            fl = MAXFNAME;       // truncate file name
        }
        memcpy(c->fname, d_name, fl);
        c->fname[fl] = '\0';

        LOGD("found audio file \"%s\" in directory \"%s\"\n", d_name, path);
        if (len + strlen(d_name) + 1 < MAXPATH)
        {
            if (*nFilesInDir == 0)
            {
                c->nDirectoriesFound++;
            }
            (*nFilesInDir)++;

            if (c->db != nullptr)
            {
                char *p = path + len;       // end of current path
                if ((len > 0) && (p[-1] != '/'))
                {
                    // path is not empty and does not end with "/"
                    *p++ = '/';
                }
                strcpy(p, d_name);   // build combined path

                if (c->mode == cmdJniScanCollectAllAudioFiles)
                {
                    //
                    // insert files
                    //

                    memset(&c->info, 0, sizeof(c->info));
                    c->info.id = c->base_id + (c->curr_id++);
                    c->info.path = path;
                    c->info.title = c->fname;           // default value
                    c->info.album = dirname;        // default value

                    switch (audioType)
                    {
                        case eMp3:
                            handleMp3(c);
                            break;
                        case eMp4:
                            handleMp4(c);
                            break;
                        case eFlac:
                            handleFlac(c);
                            break;
                        case eOgg:
                            handleOggVorbis(c);
                            break;
                        default:
                            handleMp3(c);        // make lint happy...
                            break;
                    }

                    initFileInfoFromTagsAndStat(&c->info);
                    executeSqlInsertToFileTable(c, &c->info);
                    freeTags(&c->info);                // deallocate strings
                    c->info.album = nullptr;    // bug in Android Studio, this should prevent "value escapes local scope"
                }
                else if (c->mode == cmdJniScanCollectNewAudioFiles)
                {
                    //
                    // just collect files for later insertion
                    //

                    int64_t id;
                    (void) runSqlQueryFileFromPath(c, path, &id, nullptr, nullptr,
                                                   nullptr);    // already in table?
                    if (id == 0)
                    {
                        LOGI("new file found: \"%s\"\n", path);
                        handleSingleAudioFile(path, true, c);   // increments c->nAudioFilesFound
                        doProgressCallback(c, eResultProgress);
                    }
                }
                else
                {
                    assert(0);
                }

                path[len] = '\0';       // restore old path
            }
            else
            {
                //
                // just count files
                //

                c->nAudioFilesFound++;
                if (c->jmethodProgress != nullptr)
                {
                    // call Java from C++
                    c->jenv->CallVoidMethod(c->jobj, c->jmethodProgress, c->nAudioFilesFound,
                                            c->nDirectoriesFound, c->nAlbumsFound,
                                            c->nFolderImagesCreated,
                                            c->nExistingFolderImagesScaled);
                }
            }
        }
    }
}


/************************************************************************************
 *
 * scan a path for audio files
 *
 ***********************************************************************************/
static void scanPathRecursively(char *path, Context *c)
{
    char dirname[MAXFNAME + 1];      // used in case of undefined album

    DIR *d = opendir(path);
    if (d == nullptr)
    {
        LOGE("opendir(\"%s\") -> %s\n", path, strerror(errno));
        return;
    }
    int nFilesInDir = 0;

    // get directory name as fallback solution for missing album information
    long lp = (long) strlen(path);
    const char *stp;
    extractDirNameFromPath(path, lp, dirname, &stp);
    //LOGI("opendir() directory name is \"%s\"\n", dirname);

    int cdnum = checkDirNameIsCdNumber(dirname, (int) strlen(dirname));
    if (cdnum > 0)
    {
        char super_dirname[MAXFNAME + 1];
        const char *sstp;
        extractDirNameFromPath(path, stp - path, super_dirname, &sstp);
        //LOGI("opendir() super directory name is \"%s\"\n", super_dirname);
        //LOGI("opendir() super directory is \"%.*s\"\n", (int) (stp - path), path);
    }

    struct dirent *entry;
    size_t len = strlen(path);
    while ((entry = readdir(d)) != nullptr)
    {
        if (entry->d_type & DT_DIR) // NOLINT(hicpp-signed-bitwise)
        {
            // subdirectory, skip "." and ".."
            if (strcmp(entry->d_name, "..") && strcmp(entry->d_name, ".")) // NOLINT(bugprone-suspicious-string-compare)
            {
                // remembered old path (len), build new path, do recursion, restore old path
                if (len + strlen(entry->d_name) + 1 < MAXPATH)
                {
                    char *p = path + len;       // end of current path
                    if ((len > 0) && (p[-1] != '/'))
                    {
                        // path is not empty and does not end with "/"
                        *p++ = '/';
                    }
                    strcpy(p, entry->d_name);   // build combined path
                    scanPathRecursively(path, c);             // recursion
                    path[len] = '\0';       // restore old path
                }
                else
                {
                    LOGW("maximum path depth exceeded. Ignore directory \"%s\" in \"%s\"\n", path, entry->d_name);
                }
            }
        } else
        if (!strcmp(entry->d_name, ".nomedia"))
        {
            LOGI("found .nomedia file in directory \"%s\", skipping following entries.\n", path);
            break;
        }
        else
        if (c->curr_id < MAXFILES)
        {
            // file
            scanHandleFile(path, c, dirname, len, &nFilesInDir, entry->d_name);
        }
        else
        {
            LOGW("maximum path depth exceeded. Ignore file \"%s\" in \"%s\"\n", path, entry->d_name);
        }
    }

    (void) (closedir(d));
}


/******************************************************************************
 *
 * progress callback
 *
 * msg == 0: progress
 *        <0: failure
 *        >0: success and final result
 *
 *****************************************************************************/
void doProgressCallback(Context *c, int msg)
{
    if (msg == eResultAlbumPicturesUpdated)
        c->jenv->CallVoidMethod(c->jobj, c->jmethodProgress, msg,
                c->nAlbumPicturesAdded,       // passed as arg1 to Java
                c->nAlbumPicturesRemoved,     // arg2
                c->nExistingFolderImagesScaled);
    else
        c->jenv->CallVoidMethod(c->jobj, c->jmethodProgress, msg,
                c->nAudioFilesFound,              // passed as arg1 to Java
                c->nDirectoriesFound,             // arg2
                c->nAlbumsFound,                  // arg3
                c->nFolderImagesCreated,          // arg4
                c->nFolderImagesCreationFailed,   // arg5
                c->nExistingFolderImagesScaled);
}


/******************************************************************************
 *
 * init base ID from current time
 *
 *****************************************************************************/
static void initContext(Context *c, JNIEnv* env, jobject obj)
{
    memset(c, 0, sizeof(*c));
    jclass theJavaClass = env->GetObjectClass(obj);
    c->jenv = env;
    c->jobj = obj;
    c->jmethodProgress = env->GetMethodID(theJavaClass, "jni_callback_progress", "(IIIIIII)V" /* signature */);
    c->jmethodScalePicture = env->GetMethodID(theJavaClass, "jni_callback_scale_picture", "(Ljava/lang/String;I)I" /* signature */);

#if 1
    struct timespec tp; // NOLINT(cppcoreguidelines-pro-type-member-init,hicpp-member-init)
    int rc = clock_gettime(/*CLOCK_MONOTONIC_RAW*/CLOCK_REALTIME, &tp);
    if (rc)
    {
        LOGE("Can't get clock\n");
        c->base_id = 4711; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }
    else
    {
        //LOGI("clock is %ld s and %ld ns\n", tp.tv_sec, tp.tv_nsec);
        c->base_id = tp.tv_sec;
    }
#else
    // debug code
    c->base_id = 1;
#endif
    c->curr_id = 0;
}


/******************************************************************************
 *
 * JNI function to scan a directory recursively
 *
 *****************************************************************************/
JNIFN(jint, scanDirectoryJNI)
(
    JNIEnv* env,
    jobject obj /* this */,
    jint mode,
    jstring jdbpath,                // null: just count
    jstring jmediapaths,            // paths separated by '\n'
    jstring jalbumpicturefilename   // base file name for extracted album pictures
)
{
    lastResult[0] = '\0';
    const char *dbpath = nullptr;
    const char *mediapaths = (jmediapaths != nullptr) ? (env->GetStringUTFChars(jmediapaths, nullptr)) : nullptr;
    const char *albumfname = (jalbumpicturefilename != nullptr) ? (env->GetStringUTFChars(jalbumpicturefilename, nullptr)) : nullptr;
    STRINGCOPY(album_picture_base_fname, albumfname);

    // initialise context and find Java callback
    // (I)V means: void fn(int arg)
    Context c;
    initContext(&c, env, obj);
    c.mode = mode;

    int rc;
    int msg = eResultDone;

    if (jdbpath != nullptr)
    {
        dbpath = env->GetStringUTFChars(jdbpath, nullptr);

        rc = sqlite3_open(dbpath, &c.db);     // create, if file does not exist
        if (rc)
        {
            LOGE("Can't open database: %s\n", sqlite3_errmsg(c.db));
            rc = -1;
            msg = eResultCannotOpenDb;
        }
        else
        if ((mode == cmdJniScanCollectAllAudioFiles) || (mode == cmdJniScanCollectNewAudioFiles) || (mode == cmdJniScanUpdateSpecifiedAudioFiles))
        {
            rc = createSqlTableMusicFiles(&c);
            if (!rc)
            {
                prepareSqlInsertToFileTable(&c);
                if (c.ins_stmt == nullptr)
                {
                    LOGE("cannot prepare INSERT statement\n");
                    rc = -2;
                    msg = eResultSqlError;
                }
            }
        }
    }
    else
    {
        assert(mode == cmdJniScanCountAllAudioFiles);
        rc = 0;
    }

    //
    // split up search path list and search each path separately
    //

    if ((!rc) && (mediapaths != nullptr))
    {
        char mypath[MAXPATH + 1];

        const char *sm = mediapaths;
        do
        {
            const char *s = strchr(sm, '\n');
            if (s == nullptr)
            {
                // no more separators
                s = sm + strlen(sm);
            }
            size_t len = s - sm;
            if (len > MAXPATH)
            {
                LOGE("ERR: Search path too long\n");
                msg = eResultMalformedPath;
            }
            else
            {
                memcpy(mypath, sm, len);
                mypath[len] = '\0';
                bool bExists = true;

                // normalise path to avoid double entries caused by aliases and symbolic links
                {
#if 0
                    char rpath[PATH_MAX + 1];
                    const char *rs = realpath(mypath, rpath);       // use pre-allocated new path
                    if (rs == NULL)
                    {
                        LOGE("ERR: realpath() reported an error in \"%s\"\n", rpath);
                        if (mode == 0)
                        {
                            goto err;
                        }
                    }
                    if (strlen(rs) > MAXPATH)
                    {
                        LOGE("ERR: realpath() created path that is too long: \"%s\"\n", rs);
                        goto err;
                    }
                    if (strcmp(mypath, rs))
                    {
                        LOGI("path \"%s\" was evaluated to \"%s\"\n", mypath, rs);
                        strcpy(mypath, rpath);
                    }
#else
                    const char *rs = realpath(mypath, nullptr);        // allocated memory for new path
                    if (rs == nullptr)
                    {
                        if (mode == cmdJniScanUpdateSpecifiedAudioFiles)
                        {
                            LOGI("WRN: realpath() reported an error, going to remove from db\n");
                            bExists = false;
                        }
                        else
                        {
                            LOGE("ERR: realpath() reported an error\n");
                            msg = eResultMalformedPath;
                            goto err;
                        }
                    }

                    if ((rs != nullptr) && strlen(rs) > MAXPATH)
                    {
                        LOGE("ERR: realpath() created path that is too long: \"%s\"\n", rs);
                        msg = eResultMalformedPath;
                        goto err;
                    }
                    if ((rs != nullptr) && strcmp(mypath, rs)) // NOLINT(bugprone-suspicious-string-compare)
                    {
                        LOGI("path \"%s\" was evaluated to \"%s\"\n", mypath, rs);
                        strcpy(mypath, rs);
                        free((void *) rs);
                    }
#endif
                }

                if (mode == cmdJniScanUpdateSpecifiedAudioFiles)
                {
                    handleSingleAudioFile(mypath, bExists, &c);
                    c.nAudioFilesFound++;
                    doProgressCallback(&c, eResultProgress);
                }
                else
                {
                    LOGI("start scanning path \"%s\"...\n", mypath);
                    int n = c.nAudioFilesFound;
                    scanPathRecursively(mypath, &c);
                    LOGI("... %d audio files found in path.\n", c.nAudioFilesFound - n);
                }
            }

            err:
            sm = s;
            while(*sm == '\n')
            {
                sm++;
            }
        }
        while(*sm);

        LOGD("done scanning\n");
    }

    if (c.ins_stmt != nullptr)
    {
        freeSqlInsert(&c);
    }

    if (c.db != nullptr)
    {
        sqlite3_close(c.db);
    }

    if (albumfname != nullptr)
    {
        env->ReleaseStringUTFChars(jalbumpicturefilename, albumfname);
    }

    if (mediapaths != nullptr)
    {
        env->ReleaseStringUTFChars(jmediapaths, mediapaths);
    }

    if (dbpath != nullptr)
    {
        env->ReleaseStringUTFChars(jdbpath, dbpath);
    }

    if (mode == cmdJniScanCountAllAudioFiles)
    {
        sprintf(lastResult, "%d audio files found in %d directories.", c.nAudioFilesFound, c.nDirectoriesFound);
        if (msg >= 0)
        {
            msg = eResultFileScan;
        }
    }
    else
    {
        sprintf(lastResult, "%d audio files processed.", c.nAudioFilesFound);
        if (msg >= 0)
        {
            msg = (mode == 1) ? eResultAlbumScan : eResultFileProcess;
        }
    }

    doProgressCallback(&c, msg);

    if (rc == 0)
    {
        rc = c.nAudioFilesFound;
    }

    return rc;
}


/******************************************************************************
 *
 * JNI function query table
 *
 *****************************************************************************/
JNIFN(jint, doTableJNI)
(
    JNIEnv* env,
    jobject obj,
    jstring jdbpath,
    jint command,
    jint param1,
    jstring jalbumpicturefilename   // base file name for extracted album pictures
)
{
    lastResult[0] = '\0';
    const char *dbpath = env->GetStringUTFChars(jdbpath, nullptr);
    const char *albumfname = (jalbumpicturefilename != nullptr) ? (env->GetStringUTFChars(jalbumpicturefilename, nullptr)) : nullptr;
    STRINGCOPY(album_picture_base_fname, albumfname);

    LOGI("start handling database file \"%s\" with command %d\n", dbpath, command);

    Context c;
    initContext(&c, env, obj);

    int msg = eResultDone;
    int rc;
    rc = sqlite3_open(dbpath, &c.db);     // create, if file does not exist
    if (rc)
    {
        LOGE("Can't open database: %s\n", sqlite3_errmsg(c.db));
        sprintf(lastResult, "ERROR: cannot open database \"%s\"\n (%s)", dbpath, sqlite3_errmsg(c.db));
        rc = -1;
        msg = eResultCannotOpenDb;
    }
    else
    {
        LOGD("Opened database successfully\n");

        switch (command)
        {
            case cmdJniCreateDb:
                sprintf(lastResult, "Database created/opened successfully.");
                msg = eResultDbCreatedOrOpened;
                break;

            case cmdJniQueryAudioFiles:
                runSqlQueryAllFiles(&c);
                break;

            case cmdJniQueryAlbums:
                runSqlQueryAllAlbums(&c);
                break;

            case cmdJniUpdateAlbumPictures:
                rc = runSqlUpdateAllAlbumPictures(&c);
                msg = eResultAlbumPicturesUpdated;
                break;

            case cmdJniCollectAlbums:
                createSqlTableAlbums(&c);
                runSqlCreateAllAlbumsFromFiles(&c, param1);
                sprintf(lastResult, "%d albums found from %d audio files.\n",
                        c.nAlbumsFound, c.nAudioFilesFound);
                if (c.nFolderImagesCreated)
                {
                    sprintf(lastResult + strlen(lastResult), "\n %d folder images extracted.", c.nFolderImagesCreated);
                }
                if (c.nExistingFolderImagesScaled)
                {
                    sprintf(lastResult + strlen(lastResult), "\n %d existing folder images downscaled.", c.nExistingFolderImagesScaled);
                }
                msg = eResultAlbumScan;
                break;

            case cmdJniTruncateAudioFileTable:
                truncateSqlTable(&c, FILES_TABLE_NAME);
                break;

            case cmdJniTruncateAlbumTable:
                truncateSqlTable(&c, ALBUMS_TABLE_NAME);
                break;

            case cmdJniRemoveTables:
                removeSqlTable(&c, INFO_TABLE_NAME);
                removeSqlTable(&c, FILES_TABLE_NAME);
                removeSqlTable(&c, ALBUMS_TABLE_NAME);
                sprintf(lastResult, "Existing tables removed, if any.");
                msg = eResultTablesRemoved;
                break;

            case cmdJniGetNumAudioFiles:
                rc = getNumRowsOfSqlTable(&c, FILES_TABLE_NAME);
                break;

            case cmdJniGetNumAlbums:
                rc = getNumRowsOfSqlTable(&c, ALBUMS_TABLE_NAME);
                break;

            case cmdJniWriteDbVersion:
                rc = createAndSetSqlTableInfo(&c);
                msg = eResultDone;
                break;

            case cmdJniGetDbVersion:
                rc = getDbVersionFromSqlTableInfo(&c);
                break;

            case cmdJniCheckDbVersion:
                rc = getDbVersionFromSqlTableInfo(&c);
                if (rc == DB_VERSION)
                {
                    rc = 0;
                }
                else
                if (rc == 0)
                {
                    rc = 1;
                }
                break; // NOLINT(readability-misleading-indentation)

            case cmdJniAutoScan:
                {
                    //
                    // query changed and deleted audio files (from table)
                    //

                    int all;
                    int chg;
                    struct pathList *theList = runSqlQueryChangedFiles(&c, &all, &chg);
                    LOGI("Changed audio files: %d/%d\n", chg, all);
                    // if number of changed files is high, do a total rescan?
                    bool bTooMany = (chg > all / 4);
                    if (bTooMany)
                    {
                        LOGW("Changed audio files: %d/%d are too many, giving up and recommending rescan\n",
                             chg, all);
                        rc = 1;
                    }
                    else
                    {
                        rc = 0;
                    }

                    prepareSqlInsertToFileTable(&c);
                    while (theList != nullptr)
                    {
                        if (!bTooMany)
                        {
                            handleSingleAudioFile((char *) theList->path, theList->bExists, &c);
                        }
                        struct pathList *pNext = theList->pNext;
                        free((void *) theList->path);
                        free(theList);
                        theList = pNext;
                    }
                    if (c.ins_stmt != nullptr)
                    {
                        freeSqlInsert(&c);
                    }
                    c.nAudioFilesFound = chg;
                    msg = eResultFileProcess;
                }
                break;

            default:
                LOGE("invalid command %d\n", command);
                msg = eResultInvalidCmd;
                rc = -2;
        }

        sqlite3_close(c.db);
    }

    doProgressCallback(&c, msg);

    if (albumfname != nullptr)
    {
        env->ReleaseStringUTFChars(jalbumpicturefilename, albumfname);
    }

    env->ReleaseStringUTFChars(jdbpath, dbpath);

    return rc;
}
