/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MEDIASCANNER_NATIVE_LIB_DB_H
#define MEDIASCANNER_NATIVE_LIB_DB_H

struct pathList
{
    const char *path;       // dynamically allocated
    bool bExists;
    pathList *pNext;        // next element
};

extern int createSqlTableMusicFiles(Context *c);
extern int createSqlTableAlbums(Context *c);
extern int truncateSqlTable(Context *c, const char *table_name);
extern int removeSqlTable(Context *c, const char *table_name);
extern int getNumRowsOfSqlTable(Context *c, const char *table_name);
extern void prepareSqlInsertToFileTable(Context *c);
//extern void prepareSqlInsertToAlbumTable(Context *c);
extern int executeSqlInsertToFileTable(Context *c, const audioFileInfo *t);
extern void freeSqlInsert(Context *c);
extern void runSqlQueryAllFiles(Context *c);
extern void runSqlQueryAllAlbums(Context *c);
extern int runSqlUpdateAllAlbumPictures(Context *c);
extern void runSqlCreateAllAlbumsFromFiles(Context *c, int extractFolderIcons);
extern void runSqlCreateAlbumFromFiles(Context *c, int64_t album_id, const char *album_name, const char *album_artist, int extractFolderIcons);
extern int createAndSetSqlTableInfo(Context *c);
extern int getDbVersionFromSqlTableInfo(Context *c);
extern int runSqlQueryFileFromPath(Context *c, const char *path, int64_t *p_id, int64_t *p_album_id, const char **palbum, const char **palbum_artist);
extern int runSqlQueryAlbum(Context *c, const char *album_name, const char *album_artist, int64_t *p_album_id);
extern int removeIdFromSqlTable(Context *c, const char *table_name, int64_t id);
//extern int removeFromAlbumsSqlTable(Context *c, const char *album, const char *album_artist);
extern int64_t getMaxAlbumId(Context *c);
extern int64_t getMaxFileId(Context *c);
extern pathList *runSqlQueryChangedFiles(Context *c, int *p_all, int *p_chg);

#endif //MEDIASCANNER_NATIVE_LIB_DB_H
