/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.mediascanner;

//import android.util.Log;

public class ScannerManager
{
    //private static final String LOG_TAG = "CMS : MainActivity";
    // Used to load the 'native-lib' library on application startup.
    static
    {
        System.loadLibrary("native-lib");
    }
    private final static int minimumFolderImageSize = 256;
    private final static int defaultFolderImageSize = 500;
    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    //public native String stringFromJNI();
    public native int scanDirectoryJNI(int mode, final String dbpath, final String mediapaths, final String jalbumpicturefilename);
    public native int doTableJNI(final String dbpath, int command, int param1, final String jalbumpicturefilename);

    // commands for scanDirectoryJNI()
    final static int cmdJniScanCountAllAudioFiles = 0;
    final static int cmdJniScanCollectAllAudioFiles = 1;
    final static int cmdJniScanCollectNewAudioFiles = 2;
    final static int cmdJniScanUpdateSpecifiedAudioFiles = 3;

    // commands for doTableJNI()
    final static int cmdJniCreateDb = 1;
    final static int cmdJniQueryAudioFiles = 2;
    final static int cmdJniQueryAlbums = 3;
    final static int cmdJniCollectAlbums = 4;
    final static int cmdJniTruncateAudioFileTable = 5;
    final static int cmdJniTruncateAlbumTable = 6;
    final static int cmdJniRemoveTables = 7;
    final static int cmdJniGetNumAudioFiles = 8;
    final static int cmdJniGetNumAlbums = 9;
    final static int cmdJniWriteDbVersion = 10;
    final static int cmdJniGetDbVersion = 11;
    final static int cmdJniAutoScan = 12;
    final static int cmdJniCheckDbVersion = 13;
    final static int cmdJniUpdateAlbumPictures = 14;

    // keep in sync with C++ source:
    final static int eResultMalformedPath = -4;
    final static int eResultSqlError = -3;
    final static int eResultCannotOpenDb = -2;
    final static int eResultInvalidCmd = -1;
    final static int eResultProgress = 0;
    final static int eResultFileScan = 1;
    final static int eResultFileProcess = 2;
    final static int eResultDbCreatedOrOpened = 3;
    final static int eResultDone = 4;
    final static int eResultAlbumScan = 5;
    final static int eResultTablesRemoved = 6;
    final static int eResultAlbumPicturesUpdated = 7;



    /**************************************************************************
     *
     * called from JNI
     *
     * msg == 0: progress
     *        1: final result
     *        >1: sucess
     *        <0: failure
     *
     * is in fact overwritten by caller MainActivity
     *
     *************************************************************************/
    @SuppressWarnings("unused")
    public void jni_callback_progress(int msg, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6)
    {
        //Log.d(LOG_TAG, "ScannerManager() : callback");
    }


    /**************************************************************************
     *
     * called from JNI
     *
     *************************************************************************/
    @SuppressWarnings("unused")
    public int jni_callback_scale_picture(String path, int isExistingImage)
    {
        if (isExistingImage != 0)
        {
            boolean bScaleExistingImages = UserSettings.getBool(UserSettings.PREF_SCALE_EXISTING_FOLDER_IMAGES, false);
            if (!bScaleExistingImages)
            {
                return 0;
            }
        }

        ImageScaler scaler = new ImageScaler();
        int rc = scaler.open(path);
        if (rc == 0)
        {
            int n = UserSettings.getIntStoredAsString(UserSettings.PREF_MAX_SIZE_OF_FOLDER_IMAGES, defaultFolderImageSize);
            if (n < minimumFolderImageSize)
            {
                UserSettings.putVal(UserSettings.PREF_MAX_SIZE_OF_FOLDER_IMAGES, minimumFolderImageSize);
                n = minimumFolderImageSize;
            }
            boolean bKeepBackup = UserSettings.getBool(UserSettings.PREF_KEEP_BACKUP_OF_ORIGINAL_FOLDER_IMAGES, false);
            rc = scaler.scale(n, bKeepBackup);
        }
        return rc;
    }

}
