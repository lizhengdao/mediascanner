/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.mediascanner;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

@SuppressWarnings("WeakerAccess")
public class ImageScaler
{
    private static final String LOG_TAG = "ImageScaler";

    private File mOriginalFile;
    private String mDirectory;
    private Bitmap mBitmap = null;

    int open(final String path)
    {
        mOriginalFile = new File(path);
        mDirectory = mOriginalFile.getParent();

        mBitmap = BitmapFactory.decodeFile(path);
        if (mBitmap == null)
        {
            return -1;
        }

        return 0;
    }


    // 0: unchanged, 1: changed, <0: error
    private int createDownscaledImageFile(int maxDim, File fs)
    {
        int ow = mBitmap.getWidth();
        int oh = mBitmap.getHeight();
        Log.d(LOG_TAG, "scale() : current size is w = " + ow + ", h = " + oh);

        if ((ow <= maxDim) && (oh <= maxDim))
        {
            return 0;       // nothing to do
        }

        int nw, nh;
        float ratio;
        if (ow >= oh)
        {
            nw = maxDim;
            ratio = oh;
            ratio /= ow;
            nh = (int) (nw * ratio);
        }
        else
        {
            nh = maxDim;
            ratio = ow;
            ratio /= oh;
            nw = (int) (nh * ratio);
        }

        Bitmap b2 = Bitmap.createScaledBitmap(mBitmap, nw, nh, false);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        b2.compress(Bitmap.CompressFormat.JPEG,80 , outStream);

        int rc;
        try
        {
            if (fs.createNewFile())
                rc = 1;
            else
                rc = -2;
        }
        catch (Exception e)
        {
            rc = -1;
        }

        if (rc >= 0)
        {
            // write data to file
            try
            {
                FileOutputStream fo = new FileOutputStream(fs);
                fo.write(outStream.toByteArray());
                fo.close();
            }
            catch (Exception e)
            {
                rc = -3;
            }
        }

        return rc;
    }


    int scale(int maxDim, boolean bKeepBackup)
    {
        File scaledFile = new File(mDirectory + File.separator + ".folder_scaled.jpg");
        int rc = createDownscaledImageFile(maxDim, scaledFile);
        if (rc > 0)
        {
            // scaled image has been created
            try
            {
                //
                // either rename old file or delete it
                //

                if (bKeepBackup)
                {
                    String fname = mOriginalFile.getName();
                    File fn = new File(mDirectory + File.separator + fname + ".backup");
                    if (!mOriginalFile.renameTo(fn))
                    {
                        Log.e(LOG_TAG, "scale() : backup of original folder image file failed. Trying to delete.");
                        if (!mOriginalFile.delete())    // remove original file
                        {
                            Log.e(LOG_TAG, "scale() : deleting of original folder image file also failed.");
                            rc = -6;
                        }
                    }
                }
                else
                {
                    if (!mOriginalFile.delete())    // remove original file
                    {
                        Log.e(LOG_TAG, "scale() : deleting of original folder image file failed.");
                        rc = -6;
                    }
                }

                //
                // rename new file to "folder.jpg"
                //

                if (rc >= 0)
                {
                    File fn = new File(mDirectory + File.separator + "folder.jpg");
                    if (!scaledFile.renameTo(fn))
                    {
                        rc = -7;
                    }
                }
            }
            catch (Exception e)
            {
                rc = -8;
            }
        }

        return rc;
    }
}
