/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.mediascanner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

//import static android.os.Environment.getExternalStorageDirectory;
import static android.os.Environment.getExternalStorageDirectory;
import static android.os.Environment.getExternalStorageState;


public class MainActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "CMS : MainActivity";
    static final String dbfilename = "musicmetadata.db";
    private final static int MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE = 11;
    protected static final int RESULT_SETTINGS = 1;

    boolean mReadWritePermissionGranted = false;
    boolean mbAsyncTaskBusy = false;
    private String mDbPath = null;
    private String mMediaPaths = null;     // search paths, separated by '\n'
    private enum actionType
    {
        actionRebuildAll,
        actionScanDirectories,
        actionCollectMedia,
        actionQueryAudioFiles,
        actionQueryAlbums,
        actionQueryAlbumPictures,
        actionBuildAlbums,
        actionTruncateAudioFileTable,
        actionTruncateAlbumTable,
        actionRemoveTables,
        actionGetNumbers,
        actionUpdateFiles,
        actionAutoScan,
        actionWriteDbVersion
    }

    FloatingActionButton mFloatingButton1;
    FloatingActionButton mFloatingButton2;
    TextView mText;
    ProgressBar mProgressBar;
    Timer mTimer;
    MyTimerTask mTimerTask;
    private static final int timerFrequency = 300;     // milliseconds
    int mAudioFiles;        // for progress calculation
    int mCallbackArgAudioFilesProcessed;
    int mCallbackArgDirectories;
    int mCallbackArgAlbums;
    int mCallbackArgPicturesExtracted;
    int mCallbackArgPicturesExtractionFailed;
    int mCallbackArgFolderPicturesScaled;

    int mCallbackArgAudioFilesProcessedFinal;
    String mStartStatusText;
    String mStatusText;
    String mFinalStatusText = "";
    ArrayList<String> mAudioPathArrayList = null;


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //HACK
        Intent intent = getIntent();
        mAudioPathArrayList = intent.getStringArrayListExtra("pathTable");

        super.onCreate(savedInstanceState);
        UserSettings.setContext(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);     // TODO: allow
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (!mReadWritePermissionGranted)
        {
            askPermission();
        }

        mStartStatusText = getString((mReadWritePermissionGranted) ? R.string.str_start_button : R.string.str_no_access);
        mStatusText = mStartStatusText;
        mFloatingButton1 = findViewById(R.id.fab);
        mFloatingButton2 = findViewById(R.id.fab2);
        mText = findViewById(R.id.sample_text);
        mProgressBar = findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.INVISIBLE);

        updateFloatingButton();
        mFloatingButton1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.d(LOG_TAG, "onCreate() : scan directory");

                if (initDefaultPaths(false))
                {
                    runAsyncCommand(actionType.actionAutoScan);
                }
            /*
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
                    */
            }
        });

        mFloatingButton2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.d(LOG_TAG, "onCreate() : scan directory");

                if (initDefaultPaths(false))
                {
                    runAsyncCommand(actionType.actionRebuildAll);
                }
            /*
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
                    */
            }
        });

        evalIntent();
    }


    /**************************************************************************
     *
     * called after onCreate() or after the application has been put back to
     * the foreground.
     * After onStart() the method onResume() is called.
     *
     *************************************************************************/
    @Override
    protected void onStart()
    {
        Log.d(LOG_TAG, "onStart()");

        // manage progress bar via timer
        mTimerTask = new MyTimerTask();

        //delay 1000ms, repeat in <timerFrequency>ms
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, 1000, timerFrequency);

        super.onStart();
        Log.d(LOG_TAG, "onStart() - done");
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    protected void onStop()
    {
        Log.d(LOG_TAG, "onStop()");

        // stop timer
        mTimer.cancel();    // note that a cancelled timer cannot be re-scheduled. Why not?
        mTimer = null;
        mTimerTask = null;

        super.onStop();
        Log.d(LOG_TAG, "onStop() - done");
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        final int[] debugItems =
        {
            R.id.action_query_audio_files,
            R.id.action_scan_directories,
            R.id.action_collect_files,
            R.id.action_query_albums,
            R.id.action_query_album_pictures,
            R.id.action_build_albums,
            R.id.action_truncate_audio_file_table,
            R.id.action_truncate_album_table,
            R.id.action_remove,
            R.id.action_delete_db,
            R.id.action_get_num,
            R.id.action_write_db_version
        };

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        boolean bShowDebugMenuEntries = UserSettings.getBool(UserSettings.PREF_SHOW_DEBUG_MENU_ENTRIES, false);
        if (!bShowDebugMenuEntries)
        {
            for (int item_no : debugItems)
            {
                MenuItem theMenuItem = menu.findItem(item_no);
                theMenuItem.setVisible(false);
            }
        }
        return true;
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        boolean bPaths = initDefaultPaths(false);

        switch(id)
        {
            case R.id.action_about:
                DialogAbout();
                return true;

            case R.id.action_changes:
                DialogChanges();
                break;

            case R.id.action_help:
                DialogHelp();
                break;

            case R.id.action_settings:
                Intent i = new Intent(this, MyPreferenceActivity.class);
                startActivityForResult(i, RESULT_SETTINGS);
                return true;

            case R.id.action_reset_settings:
                UserSettings.removeVal(UserSettings.PREF_MUSIC_BASE_PATH);
                UserSettings.removeVal(UserSettings.PREF_DATA_BASE_PATH);
                initDefaultPaths(true);
                return true;

            case R.id.action_scan_directories:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionScanDirectories);
                }
                return true;

            case R.id.action_collect_files:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionCollectMedia);
                }
                return true;

            case R.id.action_query_audio_files:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionQueryAudioFiles);
                }
                return true;

            case R.id.action_query_albums:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionQueryAlbums);
                }
                return true;

            case R.id.action_query_album_pictures:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionQueryAlbumPictures);
                }
                return true;

            case R.id.action_build_albums:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionBuildAlbums);
                }
                return true;

            case R.id.action_truncate_audio_file_table:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionTruncateAudioFileTable);
                }
                return true;

            case R.id.action_truncate_album_table:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionTruncateAlbumTable);
                }
                return true;

            case R.id.action_remove:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionRemoveTables);
                }
                return true;

            case R.id.action_get_num:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionGetNumbers);
                }
                return true;

            case R.id.action_write_db_version:
                if (bPaths)
                {
                    runAsyncCommand(actionType.actionWriteDbVersion);
                }
                return true;

            case R.id.action_delete_db:
                if (bPaths)
                {
                    File f = new File(mDbPath);
                    if (f.delete())
                    {
                        Log.d(LOG_TAG, "database file \"" + mDbPath + "\" deleted.");
                    }
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /************************************************************************************
     *
     * Activity method: got new intent
     *
     ***********************************************************************************/
    @Override
    protected void onNewIntent(Intent intent)
    {
        Log.d(LOG_TAG, "onNewIntent(): intent action is " + intent.getAction());
        evalIntent();
        super.onNewIntent(intent);
    }


    /**************************************************************************
     *
     * called when tracks activity or settings activity has ended
     *
     *************************************************************************/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        //noinspection SwitchStatementWithTooFewBranches
        switch (requestCode)
        {
            case RESULT_SETTINGS:
                // check for changed settings, and apply them
                initDefaultPaths(true);     // re-read db and music paths from settings
                break;
        }
    }


    /************************************************************************************
     *
     * evaluate intent
     *
     ***********************************************************************************/
    protected void evalIntent()
    {
        Intent intent = getIntent();
        mAudioPathArrayList = intent.getStringArrayListExtra("pathTable");
        if (mAudioPathArrayList != null)
        {
            if (initDefaultPaths(false))
            {
                runAsyncCommand(actionType.actionUpdateFiles);
            }
        }
    }


    /**************************************************************************
     *
     * dialogue
     *
     *************************************************************************/
    private void DialogAbout()
    {
        UserSettings.AppVersionInfo info = UserSettings.getVersionInfo(this);

        final String strTitle = getString(R.string.app_name);
        final String strDescription = getString(R.string.str_app_description);
        final String strAuthor = getString(R.string.str_author);
        final String strVersion = "Version " + info.versionName + ((info.isDebug) ? " DEBUG" : "");

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(strTitle);
        alertDialog.setIcon(R.drawable.app_icon_noborder);
        alertDialog.setMessage(
                strDescription + "\n\n" +
                        strAuthor + "Andreas Kromke" + "\n\n" +
                        strVersion + "\n" +
                        "(" + info.strCreationTime + ")");
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogHtml(final String filename)
    {
        WebView webView = new WebView(this);
        webView.loadUrl("file:///android_asset/html-" + getString(R.string.locale_prefix) + "/" + filename);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(webView);
        alertDialog.show();
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogHelp()
    {
        DialogHtml("help.html");
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogChanges()
    {
        DialogHtml("changes.html");
    }


    /**************************************************************************
     *
     * ask the operating system for the default paths for music files
     * and for the database to maintain
     *
     *************************************************************************/
    private boolean initDefaultPaths(boolean bForce)
    {
        if ((!bForce) && (mMediaPaths != null) && (mDbPath != null))
        {
            return true;        // already done
        }

        if (mReadWritePermissionGranted)
        {
            mMediaPaths = getMusicBasePath();

            mDbPath = getSharedDbBasePath();
            File dir = new File(mDbPath);
            if (!dir.isDirectory() && !dir.mkdirs())
            {
                Log.e(LOG_TAG, "getMusicBasePath(): cannot create directories");
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }


    /**************************************************************************
     *
     * ask for file access permission
     *
     *************************************************************************/
    private void askPermission()
    {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED)
        {
            Log.d(LOG_TAG, "permission immediately granted");
         //   setupPathList();
            mReadWritePermissionGranted = true;
        } else
        {
            Log.d(LOG_TAG, "request permission");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE);
        }
    }


    /**************************************************************************
     *
     * called when access request has been denied or granted
     *
     *************************************************************************/
    @Override
    public void onRequestPermissionsResult
    (
        int requestCode,
        @NonNull String[] permissions,
        @NonNull int[] grantResults
    )
    {
        Log.d(LOG_TAG, "onRequestPermissionsResult()");

        //noinspection SwitchStatementWithTooFewBranches
        switch (requestCode)
        {
            case MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE:
            {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): permission granted");
                    mReadWritePermissionGranted = true;
                    mStartStatusText = getString(R.string.str_start_button);
                    mStatusText = mStartStatusText;
                    updateFloatingButton();
//                    setupPathList();
                } else {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): permission denied");
//                    Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                }
            }
            break;

            default:
                Log.d(LOG_TAG, "onRequestPermissionsResult(): unexpected request code " + requestCode);
                break;
        }
    }


    /************************************************************************************
     *
     * method to get paths of all SD cards. Note that there is no really clean way
     * to do this.
     *
     ***********************************************************************************/
    private File[] getSdCardMusicPaths()
    {
        final String prefix = "/storage/";

        // get private (!) music paths
        File[] files = getExternalFilesDirs(Environment.DIRECTORY_MUSIC);
        // entry #0 is the internal memory, the others are SD cards.
        for (int i = 0; i < files.length; i++)
        {
            File f = files[i];
            files[i] = null;

            if (i == 0)
            {
                continue;       // remove first entry, this is the internal memory
            }

            if (f == null)
            {
                Log.e(LOG_TAG, "getSdCardMusicPaths() : got null path from OS?!?");
                continue;       // bug in this Android device?
            }

            String path = f.getPath();
            Log.d(LOG_TAG, "getSdCardMusicPaths() : found external path " + path);
            if (path.startsWith(prefix))
            {
                // find first '/' after the prefix path
                int index = path.indexOf('/', prefix.length());
                if (index >= 0)
                {
                    path = path.substring(0, index + 1) + "/Music";
                    files[i] = new File(path);
                }
            }
        }

        return files;
    }


    /* ***********************************************************************************
     *
     * debug helper
     *
     ***********************************************************************************/
    /*
    public static boolean isSymlink(File file) throws IOException
    {
        File canon;
        if (file.getParent() == null) {
            canon = file;
        } else {
            File canonDir = file.getParentFile().getCanonicalFile();
            canon = new File(canonDir, file.getName());
        }
        return !canon.getCanonicalFile().equals(canon.getAbsoluteFile());
    }
    */


    /* ***********************************************************************************
     *
     * debug helper
     *
     ***********************************************************************************/
    /*
    private void getRootPaths()
    {
        File mnt = new File("/storage");
        if (!mnt.exists())
        {
            mnt = new File("/mnt");
        }

        File[] roots = mnt.listFiles(new FileFilter()
        {
            @Override
            public boolean accept(File pathname)
            {
                Log.d(LOG_TAG, "check path " + pathname.getPath());
                boolean bIsSymlink = false;
                boolean bIsInvalid = false;
                boolean bCanWrite = pathname.canWrite();
                try
                {
                    bIsSymlink = isSymlink(pathname);
                }
                catch(Exception e)
                {
                    bIsInvalid = true;
                }

                if (bIsInvalid)
                    Log.d(LOG_TAG, "this is invalid");

                if (bIsSymlink)
                    Log.d(LOG_TAG, "this is a symlink");

                if (!bCanWrite)
                    Log.d(LOG_TAG, "cannot write");

                return pathname.isDirectory() && pathname.exists()
                        && bCanWrite && !pathname.isHidden()
                        && !bIsSymlink && !bIsInvalid;
            }
        });

        printPaths(roots, "roots");
    }
    */


    /************************************************************************************
     *
     * debug helper
     *
     ***********************************************************************************/
    @SuppressWarnings("SameParameterValue")
    private void printPaths(File[] files, final String name)
    {
        int i = 0;
        for (File f:files)
        {
            Log.d(LOG_TAG, "" + name + "[" + i + "] = " + ((f == null) ? "null" : f.getPath()));
            i++;
        }
    }


    /************************************************************************************
     *
     * get music base path from environment or from settings
     *
     * getFilesDir() and getCacheDir() return the internal private paths,
     *  i.e. /data/user/0/de.kromke.andreas.mediascanner/files and .../cache
     *
     * getExternalFilesDir(Environment.DIRECTORY_MUSIC) also returns some
     *  internal private path, i.e. /storage/emulated/0/Android/data/de.kromke.andreas.mediascanner/files/Music
     *
     * getExternalFilesDirs() does the same, but also returns a second path, i.e.
     *  /storage/42B5-1A05/Android/data/de.kromke.andreas.mediascanner/files/Music
     *
     * getExternalStorageDirectory() returns the root of the internal public
     *  memory, i.e.: /storage/emulated/0
     *
     * System.getenv("SECONDARY_STORAGE") seems only to work on Samsung devices.
     *
     ***********************************************************************************/
    private String getMusicBasePath()
    {
        File[] files;

        // methods from Activity

        /*
        Log.d(LOG_TAG, "getFilesDir(): " + getFilesDir().getPath());
        Log.d(LOG_TAG, "getCacheDir(): " + getCacheDir().getPath());
        Log.d(LOG_TAG, "getExternalFilesDir(Environment.DIRECTORY_MUSIC): " + getExternalFilesDir(Environment.DIRECTORY_MUSIC).getPath());
        //Log.d(LOG_TAG, "getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS): " + getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getPath());
        Log.d(LOG_TAG, "getExternalStorageDirectory(): " + getExternalStorageDirectory().getPath());

        // methods from Environment

        Log.d(LOG_TAG, "System.getenv(\"SECONDARY_STORAGE\") : " + System.getenv("SECONDARY_STORAGE"));
        Log.d(LOG_TAG, "Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC): " + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getPath());
        files = getExternalFilesDirs(null);
        printPaths(files, "getExternalFilesDirs(null)");
        files = getExternalFilesDirs(Environment.DIRECTORY_MUSIC);
        printPaths(files, "getExternalFilesDirs(Environment.DIRECTORY_MUSIC)");

        // brute force
        getRootPaths();
        */

        //
        // get default value from environment
        //

        File basePath;
        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        } else
        {
            basePath = new File("/");
        }

        files = getSdCardMusicPaths();
        files[0] = basePath;
        printPaths(files, "music paths");

        // concat paths
        String defaultPath = "";
        boolean isFirst = true;
        for (File f:files)
        {
            if (f != null)
            {
                if (isFirst)
                {
                    isFirst = false;
                } else
                {
                    //noinspection StringConcatenationInLoop
                    defaultPath += "\n";
                }
                //noinspection StringConcatenationInLoop
                defaultPath += f.getPath();
            }
        }

        return UserSettings.getAndPutString(UserSettings.PREF_MUSIC_BASE_PATH, defaultPath);
    }


    /************************************************************************************
     *
     * get db base path from environment or from settings
     *
     ***********************************************************************************/
    private String getSharedDbBasePath()
    {
        File basePath;

        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = getExternalStorageDirectory();
        } else
        {
            basePath = getFilesDir();
        }

        String defaultPath = basePath.getPath() + "/ClassicalMusicDb";
        return UserSettings.getAndPutString(UserSettings.PREF_DATA_BASE_PATH, defaultPath);
    }


    /**************************************************************************
     *
     * update action bar
     *
     *************************************************************************/
    void updateFloatingButton()
    {
        if (mFloatingButton1 != null)
        {
            int id = (mbAsyncTaskBusy) ? R.color.action_button_scan_busy_colour : R.color.action_button_scan_colour;
            ColorStateList tint = AppCompatResources.getColorStateList(this, id);
            mFloatingButton1.setBackgroundTintList(tint);
            if (mStatusText.isEmpty())
            {
                //Log.d(LOG_TAG, "updateFloatingButton() : status is empty");
                mText.setText(mStartStatusText);
            } else
            {
                //Log.d(LOG_TAG, "updateFloatingButton() : status is \"" + mStatusText + "\"");
                mText.setText(mStatusText);
            }
        }

        if (mFloatingButton2 != null)
        {
            int id = (mbAsyncTaskBusy) ? R.color.action_button_manualscan_busy_colour : R.color.action_button_manualscan_colour;
            ColorStateList tint = AppCompatResources.getColorStateList(this, id);
            mFloatingButton2.setBackgroundTintList(tint);
        }
    }


    /**************************************************************************
     *
     * async task has finished with success (0) or error
     *
     *************************************************************************/
    private void onCollectFinished(Integer result)
    {
        Log.d(LOG_TAG, "onCollectFinished(" + result + ")");
        String sresult = (result == 0) ? getString(R.string.str_complete) : getString(R.string.str_failed);
        Toast.makeText(getApplicationContext(),
                getString(R.string.str_scanning_uc) + " " + sresult, Toast.LENGTH_SHORT)
                .show();
        if (mAudioFiles > 0)
        {
            mProgressBar.setProgress(result == 0 ? 100 : 0);
        }
    }


    /**************************************************************************
     *
     * start async task for file operations
     *
     *************************************************************************/
    private void runAsyncCommand(actionType theAction)
    {
        if (mbAsyncTaskBusy)
        {
            Log.e(LOG_TAG, "runAsyncCommand() : task is busy");
        }
        else
        {
            // remember that async task is running
            mbAsyncTaskBusy = true;
            updateFloatingButton();
            if (mProgressBar != null)
            {
                mProgressBar.setProgress(0);
                mProgressBar.setVisibility(View.VISIBLE);
            }
/*
            Toast.makeText(getApplicationContext(),
                    "Start reading tags ...", Toast.LENGTH_SHORT)
                    .show();
*/
            boolean bExtractFolderIcons = UserSettings.getBool(UserSettings.PREF_EXTRACT_FOLDER_IMAGES, true);
            int iExtractFolderIcons = (bExtractFolderIcons) ? 1 : 0;
            new FileAccessTask().execute(theAction.ordinal() /* enum to int */, iExtractFolderIcons);
        }
    }


    /**********************************************************************************************
     *
     * timer callback is run once per second
     *
     *********************************************************************************************/
    static int toggle = 0;
    final static int maxtoggle = 4;
    public void TimerCallback()
    {
        if (mbAsyncTaskBusy)
        {
            String text = mStatusText;
            text += mCallbackArgAudioFilesProcessed + " " + getString(R.string.str_audio_files_processed);

            if (mCallbackArgDirectories != 0)
            {
                text += " " + getString(R.string.str_in) + " " + mCallbackArgDirectories + " " + getString(R.string.str_directories_dativ);
            }
            text += ".";
            if (mCallbackArgAlbums != 0)
            {
                text += "\n" + mCallbackArgAlbums + " " + getString(R.string.str_albums_found) + ".";
            }
            if (mCallbackArgPicturesExtracted != 0)
            {
                text += "\n" + mCallbackArgPicturesExtracted + " " + getString(R.string.str_album_pictures_extracted) + ".";
            }
            if (mCallbackArgPicturesExtractionFailed != 0)
            {
                text += "\n" + mCallbackArgPicturesExtractionFailed + " " + getString(R.string.str_album_pictures_not_extracted) + ".";
            }
            if (mCallbackArgFolderPicturesScaled != 0)
            {
                text += "\n" + mCallbackArgFolderPicturesScaled + " " + getString(R.string.str_existing_album_pictures_downscaled) + ".";
            }
            final String tanim = "///_///_///_///_///_///_///_///_///_///";
            text += "\n" + tanim.substring(maxtoggle - toggle, tanim.length() - toggle);

            toggle++;
            toggle %= maxtoggle;
            //Log.d(LOG_TAG, "mAudioFiles = " + mAudioFiles + ", mCallbackArgAudioFilesProcessed = " + mCallbackArgAudioFilesProcessed);
            if (mAudioFiles > 0)
            {
                mProgressBar.setProgress((mCallbackArgAudioFilesProcessed * 100) / mAudioFiles);
            }
            mText.setText(text);
        }
    }


    class MyTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    TimerCallback();
                }
            });
        }

    }


    /**************************************************************************
     *
     * called from JNI
     *
     * msg == 0: progress
     *        1: final result
     *        >1: success
     *        <0: failure
     *
     *************************************************************************/
    public void callbackFromScanner(int msg, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6)
    {
        mFinalStatusText = "";
        switch(msg)
        {
            // progress
            case ScannerManager.eResultProgress:
                mCallbackArgAudioFilesProcessed = arg1;
                mCallbackArgDirectories = arg2;
                mCallbackArgAlbums = arg3;
                mCallbackArgPicturesExtracted = arg4;
                mCallbackArgPicturesExtractionFailed = arg5;
                mCallbackArgFolderPicturesScaled = arg6;
                break;

            case ScannerManager.eResultMalformedPath:
                mFinalStatusText = getString(R.string.str_err_malformed_path);
                break;

            case ScannerManager.eResultSqlError:
                mFinalStatusText = getString(R.string.str_err_sql_problem);
                break;

            case ScannerManager.eResultCannotOpenDb:
                mFinalStatusText = getString(R.string.str_err_cannot_open_db);
                break;

            case ScannerManager.eResultInvalidCmd:
                mFinalStatusText = getString(R.string.str_err_invalid_cmd);
                break;

            case ScannerManager.eResultFileScan:
                mFinalStatusText = "" + arg1 + " " + getString(R.string.str_audio_files_found) + " " + getString(R.string.str_in) + " " + arg2 + " " + getString(R.string.str_directories_dativ) + ".";
                break;

            case ScannerManager.eResultFileProcess:
                mCallbackArgAudioFilesProcessedFinal = arg1;
                mFinalStatusText = "" + arg1 + " " + getString(R.string.str_audio_files_processed) + ".";
                break;

            case ScannerManager.eResultDbCreatedOrOpened:
                mFinalStatusText = getString(R.string.str_db_created_opened);
                break;

            case ScannerManager.eResultDone:
                break;

            case ScannerManager.eResultAlbumScan:
                mFinalStatusText = "" + arg3 + " " + getString(R.string.str_albums_found) + " " + getString(R.string.str_from) + " " + arg1 + " " + getString(R.string.str_audio_files) + ".\n";
                if (arg4 > 0)
                {
                    mFinalStatusText += "\n" + arg4 + " " + getString(R.string.str_album_pictures_extracted) + ".";
                }
                if (arg5 > 0)
                {
                    mFinalStatusText += "\n" + arg5 + " " + getString(R.string.str_album_pictures_not_extracted) + ".";
                }
                if (arg6 > 0)
                {
                    mFinalStatusText += "\n" + arg6 + " " + getString(R.string.str_existing_album_pictures_downscaled) + ".";
                }
                break;

            case ScannerManager.eResultTablesRemoved:
                mFinalStatusText = getString(R.string.str_tables_removed);
                break;

            case ScannerManager.eResultAlbumPicturesUpdated:
                mFinalStatusText = getString(R.string.str_album_pictures_added) + ": " + arg1 + ", " + getString(R.string.str_removed) + ": " + arg2 + ", " + getString(R.string.str_scaled) + ": " + arg3 + "\n";
                break;
        }
    }


    /**************************************************************************
     *
     * async task for file operations
     *
     *************************************************************************/
    @SuppressLint("StaticFieldLeak")
    private class FileAccessTask extends AsyncTask<Integer /* do */, Integer /* pre */, Integer /* post */>
    {
        actionType actionCode;
        int iExtractFolderIcons;

        @Override
        protected void onPreExecute()
        {
            Log.d(LOG_TAG, "onPreExecute()");
        }


        @Override
        protected Integer doInBackground(Integer... params)
        {
            actionCode = actionType.values()[params[0]];        // convert int to enum
            iExtractFolderIcons = params[1];
            String dbfile = mDbPath + "/" + dbfilename;
            String albumpicfname = UserSettings.getAlbumPicFname();

            Log.d(LOG_TAG, "doInBackground(" + actionCode + ")");

            ScannerManager scanner = new ScannerManager()
            {
                public void jni_callback_progress(int msg, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6)
                {
                    //Log.d(LOG_TAG, "doInBackground:ScannerManager() : callback");
                    callbackFromScanner(msg, arg1, arg2, arg3, arg4, arg5, arg6);
                }
            };

            int res = 0;
            switch (actionCode)
            {
                //
                // process update messages from tagger
                //

                case actionUpdateFiles:
                    // concat paths with LF
                    mAudioFiles = mAudioPathArrayList.size();
                    int i;
                    StringBuilder sb = new StringBuilder(1024);
                    for (i = 0; i < mAudioFiles; i++)
                    {
                        sb.append(mAudioPathArrayList.get(i));
                        if (i < mAudioFiles - 1)
                        {
                            sb.append('\n');
                        }
                    }

                    mStatusText = "updating:\n" + sb.toString() + "\n";

                    res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanUpdateSpecifiedAudioFiles, dbfile, sb.toString(), albumpicfname);
                    mStatusText += mFinalStatusText + "\n";
                    break;

                //
                // Rebuild everything from scratch, i.e. create database file, if necessary, and remove and recreate tables
                //
                // This will also update the db version automatically, if necessary
                //

                case actionRebuildAll:
                    res = doRebuildAll(scanner, dbfile);
                    break;

                //
                // auto scan, i.e. process changes, deletions and new files
                //

                case actionAutoScan:
                    res = doAutoScan(scanner, dbfile);
                    if (res != 0)
                    {
                        // error: do full scan instead
                        Log.d(LOG_TAG, "doInBackground(actionAutoScan) : Auto scan failed, doing full scan instead");
                        mStatusText += "\n\nProblem encountered.\n";
                        mStatusText += "Doing full scan instead...\n";
                        res = doRebuildAll(scanner, dbfile);
                    }
                    break;

                case actionScanDirectories:
                    res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCountAllAudioFiles, null, mMediaPaths, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionCollectMedia:
                    res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCollectAllAudioFiles, dbfile, mMediaPaths, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionQueryAudioFiles:
                    res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniQueryAudioFiles, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionQueryAlbums:
                    res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniQueryAlbums, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionQueryAlbumPictures:
                    res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniUpdateAlbumPictures, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionBuildAlbums:
                    res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniCollectAlbums, iExtractFolderIcons, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionTruncateAudioFileTable:
                    res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniTruncateAudioFileTable, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionTruncateAlbumTable:
                    res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniTruncateAlbumTable, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionRemoveTables:
                    res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniRemoveTables, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionWriteDbVersion:
                    res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniWriteDbVersion, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionGetNumbers:
                    scanner.doTableJNI(dbfile, ScannerManager.cmdJniGetDbVersion, 0, albumpicfname);
                    scanner.doTableJNI(dbfile, ScannerManager.cmdJniGetNumAudioFiles, 0, albumpicfname);
                    scanner.doTableJNI(dbfile, ScannerManager.cmdJniGetNumAlbums, 0, albumpicfname);
                    break;
            }

            return res;
        }


        @Override
        protected void onPostExecute(Integer result)
        {
            Log.d(LOG_TAG, "onPostExecute(" + result + ")");
            if ((actionCode == actionType.actionRebuildAll) ||
                (actionCode == actionType.actionBuildAlbums) ||
                (actionCode == actionType.actionAutoScan))
            {
                MainActivity.this.onCollectFinished(result);
            }
            mbAsyncTaskBusy = false;
            if (mFloatingButton1 == null)
            {
                // in batch mode
                System.exit(0);
            }
            updateFloatingButton();
        }


        /**************************************************************************
         *
         * async task: helper
         *
         *************************************************************************/
        private int doRebuildAll(ScannerManager scanner, final String dbfile)
        {
            int res;

            mStatusText = "";
            mAudioFiles = 0;
            String albumpicfname = UserSettings.getAlbumPicFname();

            res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCountAllAudioFiles, null, mMediaPaths, albumpicfname);
            mStatusText = mFinalStatusText + "\n";
            if (res < 0)
            {
                return -1;     // failure
            }

            mAudioFiles = res;
            res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniCreateDb, 0, albumpicfname);
            mStatusText += mFinalStatusText + "\n";
            if (res < 0)
            {
                return -2;     // failure
            }

            res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniRemoveTables, 0, albumpicfname);
            mStatusText += mFinalStatusText + "\n";
            if (res < 0)
            {
                return -3;     // failure
            }

            res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCollectAllAudioFiles, dbfile, mMediaPaths, albumpicfname);
            if (res < 0)
            {
                return -4;     // failure
            }

            res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniCollectAlbums, iExtractFolderIcons, albumpicfname);
            mStatusText += mFinalStatusText + "\n";
            if (res < 0)
            {
                return -5;     // failure
            }

            res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniWriteDbVersion, 0, albumpicfname);

            return res;
        }


        /**************************************************************************
         *
         * async task: helper
         *
         *************************************************************************/
        private int doAutoScan(ScannerManager scanner, final String dbfile)
        {
            int res;
            final String albumpicfname = UserSettings.getAlbumPicFname();

            //
            // intro: check DB version
            //

            mStatusText = "";
            mAudioFiles = 0;
            res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniCheckDbVersion, 0, albumpicfname);
            if (res < 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : Check Db version failed");
                return -1;     // failure
            }
            if (res > 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : Db version mismatch");
                mStatusText = getString(R.string.str_db_version_mismatch) + "\n";
                return - 2;
            }

            //
            // count number of audio files on device
            //

            res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCountAllAudioFiles, null, mMediaPaths, albumpicfname);
            mStatusText = mFinalStatusText + "\n";
            if (res < 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : file scan failed");
                return -3;     // failure
            }
            mAudioFiles = res;

            //
            // count number of files in table
            //

            mCallbackArgAudioFilesProcessed = 0;
            res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniGetNumAudioFiles, 0, albumpicfname);
            if (res < 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : Get audio file table length failed");
                return -4;     // failure
            }
            int nFilesInTable = res;

            //
            // if number of new or deleted files is too high, do a rebuild instead
            //

            if (mAudioFiles > nFilesInTable)
            {
                if (mAudioFiles - nFilesInTable > nFilesInTable / 4)
                {
                    mStatusText = getString(R.string.str_too_may_new_files) + "\n";
                    return 1;       // too many new files
                }
            }
            else
            {
                if (nFilesInTable - mAudioFiles > nFilesInTable / 3)
                {
                    mStatusText = getString(R.string.str_too_many_deleted_files) + "\n";
                    return 2;       // too many deleted files
                }
            }

            //
            // handle changed and deleted audio files from table
            //

            mCallbackArgAudioFilesProcessed = 0;
            mStatusText = getString(R.string.str_looking_for_chg_or_del_files) + "\n";
            res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniAutoScan, 0, albumpicfname);
            if (res < 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : looking for deleted or changed files failed");
                return -5;     // failure
            }
            if (res > 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : too many changes");
                mStatusText = "Too many changed files. Do a rebuild" + "\n";
                return 3;       // too many changed files
            }

            int changed = mCallbackArgAudioFilesProcessedFinal;
            mCallbackArgAudioFilesProcessed = 0;
            mStatusText = getString(R.string.str_changed_or_removed_files) + " " + changed + "\n";

            //
            // handle new audio files (not yet in table)
            //

            mCallbackArgAudioFilesProcessed = 0;
            String s = mStatusText; // save
            mStatusText += getString(R.string.str_looking_for_new_files) + "\n";
            res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCollectNewAudioFiles, dbfile, mMediaPaths, albumpicfname);
            if (res >= 0)
            {
                mStatusText = s + getString(R.string.str_new_files) + " " + res + "\n";
                //res = 0;        // no error
            }

            //
            // handle removed or updated album pictures
            //

            s = mStatusText; // save
            mStatusText += getString(R.string.str_checking_album_pictures) + "\n";
            res = scanner.doTableJNI(dbfile, ScannerManager.cmdJniUpdateAlbumPictures, 0, albumpicfname);
            if (res >= 0)
            {
                mStatusText = s + mFinalStatusText + "\n";
                res = 0;        // no error
            }

            return res;
        }
    }
}
