# Classical Music Scanner
<img alt="Logo" src="app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" width="80">

A plain audio file metadata extractor ("scanner") especially for **classical music**. It basically creates an SQLite database to be used by player applications like the *Unpoular Music Player* or the *Opus 1 Music Player*.

As "pop music" is an abbreviation for "popular music", any other music obviously is "unpopular", so this program could also be called *Unpopular Music Scanner*.

***

<a href='https://play.google.com/store/apps/details?id=de.kromke.andreas.mediascanner'><img src='public/google-play.png' alt='Get it on Google Play' height=45/></a>
<a href='https://f-droid.org/app/de.kromke.andreas.mediascanner'><img src='public/f-droid.png' alt='Get it on F-Droid' height=45 ></a>

***

# Music Organisation

Other music organisation apps are meant for pop music only and deal with "Songs" and "Artists". This is not suitable for classical (baroque, renaissance, ...) music which is organised by composer, work, movements and performers.

Pop music:

Artist | Album  | Song
-------| ------ | -------------
Smith | Heaven  | 1. Love you, babe
 | | 2. Babe, love you
 | | 3. More love, babe, inne wadr
Jones  | Freedom | 1. Babe, I'm free
 | | 2. Free the innrnatt

Classical music needs more metadata ("tags"), especially the *composer tag* and *grouping tag*, the latter is used to group the movements together:

| Album  | Composer  | Work | Performer | Movement
|------- |-----------| -----| --- | ---
| Cello Concertos | Joseph Haydn | Concerto No. 1 | Fred Stone | 1. Allegro
|                 |              |                |            | 2. Adagio 
|                 |              |                |            | 3. Presto 
|                 | Hella Haydn  | Concerto No. 3 | Jean Water | 1. Allegro
|                 |              |                |            | 2. Adagio


For more information see the Google Play Store entry or the offline help text of the application (if existing). Also consult the *Unpopular Music Player*.

# Screenshots
<img alt="No" src="public/Screenshot_1.png" width="320">


# Supported

* To be run manually whenever audio files have been added, removed or changed.
* Run automatically (in background) by *Classical Music Tagger* whenever audio files have been changed.
* Auto scan (incremental) or complete scan (rebuild) selectable.
* Also directly callable from player app (both *Opus 1 Music Player* (with Version 1.80) and *Unpopular Music Player* (with version 1.50)).
* Scans an arbitrary number of search paths.
* Various audio file types (mp3, mp4, flac, ogg, ...).
* Multi CD albums: sub-subdirectories "CD1", "CD2", ..., with common folder image.
* Common and non-common text tags.
* Composer and grouping (work, movements).
* Proprietary Apple iTunes tags for classical music.
* Extracting of embedded images in case there is no "folder.jpg" or "folder.png".
* For convenience "cover.jpg" is also accepted.
* Downscaling of both extracted and existing album images.
* Original images can be kept as backup, if desired.
* Maximum image size is configurable.
* Creates a standard SQLite database in "/ClassicalMusicDb" that can be accessed from PC or other apps.
* Uses native code (C++) for fast file operations.

# Not Supported (Yet)

* Album art extraction from Vorbis tags
* File system change monitoring
* Exotic album picture file names like Folder.jpg or FOLDER.JPG or Cover_front.jpg etc.

# Permissions Needed

* Read storage
* Write storage

# License

The Classical Music Scanner is licensed according to GPLv3, see LICENSE file.

# External Licenses

**TagLib C++ Library:**  
Source: https://taglib.org/  
License: LGPL (http://www.gnu.org/copyleft/lesser.html) and MPL (https://www.mozilla.org/en-US/MPL/1.1/)

**SQLite C Library:**  
Source: https://www.sqlite.org/  
License: https://www.sqlite.org/copyright.html
